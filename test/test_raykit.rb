# frozen_string_literal: true

require "simplecov"
SimpleCov.start
require "minitest/autorun"
require_relative "../lib/raykit"
# require_relative './raykit/test_command.rb'
# require_relative './raykit/test_console.rb'
# require_relative './raykit/test_environment.rb'
# require_relative './raykit/test_git.rb'
# require_relative './raykit/test_rake.rb'
# require_relative './raykit/test_version.rb'

test_root = File.dirname(File.absolute_path(__FILE__))
puts "test_root #{test_root}"
Dir.glob("#{test_root}/raykit/**/*.rb").sort.each { |file| require file }

class RaykitTest < Minitest::Test
  def test_import
    url = "https://github.com/lou-parslow/Sample.Files.git"
    REPOSITORIES.remove(url)
    assert(!REPOSITORIES.to_json.include?(url))
    REPOSITORIES.import(url)
    assert(REPOSITORIES.to_json.include?(url))
    REPOSITORIES.remove(url)
  end
end

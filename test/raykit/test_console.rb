# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"

class ConsoleTest < Minitest::Test
  def test_no_args; end

  # def test_sync_version

  #    file_a = Tempfile.new('a')
  #    file_b = Tempfile.new('b')

  #    file_a.write("<Version>1.2.3</Version>")
  #    file_a.close

  #    file_b.write("<Version>4.5.6</Version>")
  #    file_b.close

  #    ARGV.clear
  #    ARGV << 'sync_version'
  #    ARGV << file_a.path
  #    ARGV << file_b.path
  #    puts ''
  #    puts Rainbow("raykit " + ARGV.join(' ')).blue.bright

  #    console = Raykit::Console.new
  #    console.run

  #    assert_equal("<Version>1.2.3</Version>",IO.read(file_b.path))

  #    file_a.unlink
  #    file_b.unlink
  # end

  #def test_run
  #  ARGV.clear
  #  ARGV << "run"
  #  ARGV << "https://github.com/lou-parslow/Sample.Files.git"
  #end

  #def test_clean
  #  ARGV.clear
  #  ARGV << "clean"
  #  ARGV << "nuget-cs"
  #  console = Raykit::Console.new
  #  console.run
  #end
end

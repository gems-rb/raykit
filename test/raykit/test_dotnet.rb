# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"
require "tempfile"

class DotNetTest < Minitest::Test
  def test_get_package_names
    file = Tempfile.new("sample.csproj")
    file.write "<Project>\n"
    file.write '<PackageReference Include="Package.1"/>\n'
    file.write '<PackageReference Include="Package.2"/>\n'
    file.write "</Project>\n"
    file.close

    package_names = Raykit::DotNet.get_package_names(file.path)
    assert_equal(2, package_names.length)
    puts package_names
    file.unlink
  end
end

# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"

class ZipTest < Minitest::Test
  def test_run
    puts "\n===ZipTest start===\n"
    Dir.mktmpdir do |dir|
      puts "created temp dir #{dir}"
      create(dir, "data/a.txt", "a")
      create(dir, "data/b.txt", "b")
      create(dir, "data/c.txt", "c")
      create(dir, "data/d.txt", "d")
      create(dir, "data/config/config.txt", "config")

      zip_filename = "#{dir}/resources/data.zip"
      assert(!File.exist?(zip_filename), "#{zip_filename} exists")

      zip = Raykit::Zip.new(zip_filename)
                       .source_dir("#{dir}/").include_glob("**/*.txt").zip()

      zip_filename = "#{dir}/resources/data.zip"
      assert(File.exist?(zip_filename), "#{zip_filename} does not exist")

      Raykit::Zip::zip_directory(dir, "#{dir}/resources/data2.zip")
      assert(File.exist?("#{dir}/resources/data2.zip"), "#{dir}/resources/data2.zip does not exist")
    end
    puts "\n===ZipTest complete===\n"
  end

  def create(dir, rel_name, content)
    filename = "#{dir}/#{rel_name}"
    parent = File.dirname(filename)
    FileUtils.mkdir_p parent unless Dir.exist?(parent)
    File.open(filename, "w") { |f| f.write(content) }
    assert(File.exist?(filename), "file #{filename} does not exist")
    #puts "created #{filename}"
  end
end

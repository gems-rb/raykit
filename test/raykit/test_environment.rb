# frozen_string_literal: true

class EnvironmentTest < Minitest::Test
  def test_root_dir
    assert(Dir.exist?(Raykit::Environment.root_dir))
  end

  def test_get_dev_dir
    assert(Dir.exist?(Raykit::Environment.get_dev_dir("log")), Raykit::Environment.get_dev_dir("log"))
    assert(Dir.exist?(Raykit::Environment.get_dev_dir("tmp")))
    assert(Dir.exist?(Raykit::Environment.get_dev_dir("work")))
  end

  def test_get_work_dir
    dir = Raykit::Environment.get_work_dir("https://gitlab.com/gems-rb/raykit.git")
    assert(!dir.include?("https://"))
    assert(dir.include?("work"))
    assert(dir.include?("https"))
  end

  def test_local_application_data
    # assert(Dir.exist?(Raykit::Environment::local_application_data))
  end

  def test_admin
    assert(!Raykit::Environment.admin?)
  end

  def test_which
    assert(File.exist?(Raykit::Environment.which("git")))
  end
end

# frozen_string_literal: true

require "minitest/autorun"

class CommandTest < Minitest::Test
  def test_run
    cmd = Raykit::Command.new("git --version").run
    assert_equal("git --version", cmd.command, "Command.command")
    assert_equal(0, cmd.exitstatus, "Command.exitstatus")
    assert_equal("", cmd.error, "Command.error")
    assert_equal("git version", cmd.output[0..10], "Command.output")
    assert_equal(0, cmd.timeout, "Command.timeout")

    cmd = Raykit::Command.new("git --version").set_timeout(5).run
    assert_equal("git --version", cmd.command, "Command.command with timeout=5")
    assert_equal(0, cmd.exitstatus, "Command.exitstatus with timeout=5")
    assert_equal("", cmd.error, "Command.error with timeout=5")
    assert_equal("git version", cmd.output[0..10], "Command.output with timeout=5")
    assert_equal(5, cmd.timeout, "Command.timeout with timeout=5")
  end

  def test_run_error
    cmd = Raykit::Command.new("git --invalid").run #, 0, nil, false)
    assert_equal("git --invalid", cmd.command, "Command.command")
    assert_equal(129, cmd.exitstatus, "Command.exitstatus")
    assert_equal("unknown", cmd.error[0..6], "Command.error")
    assert_equal("", cmd.output, "Command.output")
    assert_equal(0, cmd.timeout, "Command.timeout")

    cmd.timeout = 5
    cmd.run
    assert_equal("git --invalid", cmd.command, "Command.command with timeout=5")
    assert_equal(129, cmd.exitstatus, "Command.exitstatus with timeout=5")
    assert_equal("unknown", cmd.error[0..6], "Command.error with timeout=5")
    assert_equal("", cmd.output, "Command.output with timeout=5")
    assert_equal(5, cmd.timeout, "Command.timeout with timeout=5")

    #cmd = Raykit::Command.new("git clone invalid").run
    #cmd.timeout = 5
    #assert_equal(128, cmd.exitstatus, "exitstatus for 'git clone invalid'")
    #assert(cmd.error.include?("fatal"), "git clone invalid, error does not include 'fatal;")
    #puts "=================="
    #cmd.summary
    #cmd.details
    #puts "=================="
  end

  def test_run_timeout
    if Gem.win_platform?
      cmd = Raykit::Command.new("ftp").set_timeout(0.1).run
      assert_equal(5, cmd.exitstatus, "Command.exitstatus")
    end
  end

  def test_to_json
    cmd = Raykit::Command.new("git --version").run
    json = JSON.generate(cmd.to_hash)
    assert(json.include?("git --version"), "json includes 'git --version'")
  end

  def test_parse
    cmd = Raykit::Command.new("git --version").run
    json = JSON.generate(cmd.to_hash)

    cmd2 = Raykit::Command.parse(json)
    assert_equal("git --version", cmd2.command)
  end

  def test_parse_yaml_commands
    yaml_filename = "#{__dir__}/../../.gitlab-ci.yml"
    yaml = File.open(yaml_filename).read
    commands = Raykit::Command.parse_yaml_commands(yaml)
    #assert_equal(15, commands.length, "commands.length")
  end
end

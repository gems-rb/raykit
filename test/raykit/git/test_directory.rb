# frozen_string_literal: true

require "minitest/autorun"

module Raykit
  module Git
    class DirectoryTest < Minitest::Test
      def test_usage
        repo = Repository.new("https://github.com/lou-parslow/Sample.Files.git")
        work = Raykit::Git::Directory.new(repo.get_dev_dir("work"))
        repo.clone(work.directory, 1) unless Dir.exist?(work.directory)
        #assert(work.user_can_commit, "work.user_can_commit") if Gem.win_platform?
        # assert_equal(false,work.outstanding_commit?,'work.outstanding_commit?')
        assert_equal("master", work.branch, "work.branch")
        assert_equal("https://github.com/lou-parslow/Sample.Files.git", work.remote, "work.remote")
        assert_equal("", work.get_tag_commit_id("unknown_tag"))
        # assert_equal('bd1396ea975090bd0ebdb15d15d044c2bd16a4e0',work.get_tag_commit_id("0.0.0"))
        # assert_equal('6ed6644a7bafcbb74875d559be7f5a7be56a1611aead467813d4597f48c1a889',work.get_sha('rakefile.rb'))
      end

      def test_src_files
        repo = Repository.new("https://github.com/lou-parslow/Sample.Files.git")
        work = Raykit::Git::Directory.new(repo.get_dev_dir("work"))
        repo.clone(work.directory, 1) unless Dir.exist?(work.directory)
        #Dir.chdir(work.directory) do
        #work.src_files.each { |f| puts f }
        #puts "================================="
        #puts "last_modified_src_time #{work.last_modified_src_time}"
        #puts "================================="
        assert_equal(59, work.src_files.length, "work.src_files.length")
        #end
      end
    end
  end
end

# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"
require "tempfile"

class LogTest < Minitest::Test
  def test_usage
    file = Tempfile.new("project.log.json")
    filename = file.path
    file.write("{}")
    file.close

    log = Raykit::Log.new(filename)
    log["name"] = "test"
    log["command_times"] = { "git --version" => "2001-02-03T04:05:06-07:00" }
    # log["commands"] = {
    #    {"git --version" =>  {"last_executed" => "2001-02-03T04:05:06-07:00"} }
    # }
    log.save

    log2 = Raykit::Log.new(filename)
    assert_equal("test", log2["name"])

    now = DateTime.now
    puts "now #{now.iso8601}"
    log2.update_command_time("git --version", now)
    command_time = log2.get_command_time("git --version")
    assert_equal(now.year, command_time.year)
    file.unlink
  end
end

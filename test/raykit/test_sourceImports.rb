# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"
require "tempfile"
require "json"

class SourceImportsTest < Minitest::Test
  def test_source_imports_usage
    file = Tempfile.new("sourceImports.json")
    filename = file.path
    sourceImports = Raykit::SourceImports.new(["https://gitlab.com/gems-rb/raykit.git"])
    sourceImports.save(filename)
    sourceImports.update
    sourceImports.save(filename)

    puts "sourceImports.json"
    puts sourceImports.to_json
    verify = Raykit::SourceImports.load(filename)
    verify.update
    assert_equal(1, verify.length)
    file.close
    file.unlink
  end
end

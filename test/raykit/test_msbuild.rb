# frozen_string_literal: true

class MsBuildTest < Minitest::Test
  def test_msbuild_path
    if Gem.win_platform?
      assert(Dir.exist?(Raykit::MsBuild::msbuild_path))
      #puts "msbuild_path #{Raykit::MsBuild::msbuild_path}"
    end
  end
end

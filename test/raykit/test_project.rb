# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"

class ProjectTest < Minitest::Test
  def test_initialize
    project = Raykit::Project.new("test", "a test project", "0.0.1")
    assert_equal("test", project.name, "project.Name")
    assert_equal("0.0.1", project.version, "project.Version")
    assert_equal("a test project", project.description, "project.Description")
  end

  def test_usage
    project = Raykit::Project.new
    project.name = "raykit"
    assert_equal("raykit", project.name, "project.Name")
    assert(Dir.exist?(project.directory))
  end

  def test_run
    project = Raykit::Project.new
    output = project.run("git --version")
    # assert_equal("[0s] git --version",output)
    # assert(project.log.get_command_time('git --version'))
  end

  def test_outstanding_commit?
    project = Raykit::Project.new
    project.outstanding_commit?
  end
end

require "minitest/autorun"

module Raykit
  module Conan
    class BuildInfoTest < Minitest::Test
      def test_usage
        puts "Dir.pwd=#{Dir.pwd}"
        buildinfo_text = IO.read("./test/raykit/conan/conanbuildinfo.txt")
        assert(buildinfo_text.include?("[builddirs]"))
        build_info = BuildInfo.new("./test/raykit/conan/conanbuildinfo.txt")
        #puts JSON.pretty_generate(build_info)
        assert_equal(3, build_info.section("[builddirs]").length, "build_info.builddirs.length")

        pdbs = build_info.pdbs
        puts JSON.pretty_generate(pdbs)
        #assert_equal(11,pdbs.length)
      end
    end
  end
end

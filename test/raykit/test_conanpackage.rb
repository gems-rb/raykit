# frozen_string_literal: true

class ConanPackageTest < Minitest::Test
  def test_new
    Raykit::ConanPackage::new("gtest", "1.10.0")
  end

  def test_get
    puts "\n===NuGetPackageTest::test_get start===\n"
    text = %q(
      requires = "gtest/1.10.0", "catch2/3.1.0@my+channel.com/branch"
      gtest/1.10.0
      )

    version = Raykit::ConanPackage::get_version(text, "gtest")
    assert_equal("1.10.0", version)

    filename = Dir.tmpdir() + "/conanfile.txt"
    File.open(filename, "w") { |f| f.puts text }
    text = Raykit::ConanPackage::set_version(text, "gtest", "1.11.0")

    puts "=============================="
    puts text
    puts "=============================="

    assert(!text.include?("1.10.0"))
    assert(text.include?("1.11.0"))

    Raykit::ConanPackage::set_version_in_file(filename, "gtest", "1.11.0")
    text = IO.read(filename)
    assert(!text.include?("1.10.0"))
    assert(text.include?("1.11.0"))
  end
end

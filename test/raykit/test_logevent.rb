# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"
require "tempfile"

class LogEventTest < Minitest::Test
  def test_usage
    logEvent = Raykit::LogEvent.new("Debug", "Raykit::LogEvent Test", {
      "Url": "https://gitlab.com/gems-rb/raykit.git",
      "Class": self.class,
      "Method": __method__,
      "Callee": __callee__,
      "File": __FILE__,
      "Line": __LINE__,
    })
    # puts "LogEvent\n"
    # puts logEvent.inspect
    # puts "JSON\n"
    # puts JSON.pretty_generate(logEvent)
    # logEvent.to_seq
  end
end

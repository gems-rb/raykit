# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"

class DirTest < Minitest::Test
  def test_get_package_names
    assert(Dir.exist?(Dir.get_data_dir))
  end
end

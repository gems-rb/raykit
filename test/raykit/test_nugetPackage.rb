# frozen_string_literal: true

class NuGetPackageTest < Minitest::Test
  def test_new
    Raykit::NuGetPackage::new("NUnit", "3.13.2")
  end

  def test_get
    puts "\n===NuGetPackageTest::test_get start===\n"
    text = %q(
    <PackageReference Include="NUnit" Version="3.13.2" />
    <Reference Include="NUnit, Version=3.13.2, Culture=neutral, processorArchitecture=MSIL">
    <HintPath>..\\packages\\NUnit.3.13.2\\lib\\net48\\NUnit.dll</HintPath>
    )
    version = Raykit::NuGetPackage::get_version(text, "NUnit")
    assert_equal("3.13.2", version)

    text = Raykit::NuGetPackage::set_version(text, "NUnit", "3.13.3")
    assert(!text.include?("3.13.2"))
  end
end

# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"
require "tempfile"

class VersionHelperTest < Minitest::Test
  def test_detect
    assert(Raykit::Version.detect("raykit").include?("0."))
  end

  # def test_sync_file_versions
  #    file_a = Tempfile.new('a')
  #    file_b = Tempfile.new('b')

  #    file_a.write("<Version>1.2.3</Version>")
  #    file_a.close

  #    file_b.write("<Version>4.5.6</Version>")
  #    file_b.close

  #    Raykit::VersionHelper::sync_file_versions(file_a.path,file_b.path)

  #    assert_equal("<Version>1.2.3</Version>",IO.read(file_b.path))

  #    file_a.unlink
  #    file_b.unlink
  # end

  # def test_sync_file_versions_wxs
  #    file_a = Tempfile.new('a')
  #    file_b = Tempfile.new('b')

  #    file_a.write("<Version>1.2.3</Version>")
  #    file_a.close

  #    file_b.write("Version=\"4.5.6\"")
  #    file_b.close

  #    Raykit::VersionHelper::sync_file_versions(file_a.path,file_b.path)

  #    assert_equal("Version=\"1.2.3\"",IO.read(file_b.path))

  #    file_a.unlink
  #    file_b.unlink
  # end

  # def test_bump
  #    assert_equal('1.0.1',Raykit::VersionHelper::bump('1.0.0'))
  # end
end

# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/raykit"

class SecretsTest < Minitest::Test
  def test_usage
    SECRETS["test"] = "abc123XYZ"
    assert_equal("abc123XYZ", SECRETS["test"])
    text = "test=abc123XYZ"
    assert_equal("test=****", SECRETS.hide(text))
    SECRETS.save
  end
end

# frozen_string_literal: true

require "simplecov"
SimpleCov.start
require_relative "../lib/raykit"
test_root = File.dirname(File.absolute_path(__FILE__))
puts "test_root #{test_root}"
Dir.glob("#{test_root}/raykit/**/*.rb").sort.each { |file| require file }

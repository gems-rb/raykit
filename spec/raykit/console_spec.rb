# frozen_string_literal: true

# require 'rspec/autorun'
require_relative "../../lib/raykit"

describe Raykit::Console do
  it "show usage when no argument are passed" do
    ARGV.clear
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run
  end

  it "presents error message when unrecognized verb is passed" do
    ARGV.clear
    ARGV << "fly"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run
  end

  it "allows a git repository url to be added" do
    ARGV.clear
    ARGV << "add"
    ARGV << "https://github.com/lou-parslow/Sample.Files.git"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run
  end

  it "allows a git repository url to be removed" do
    ARGV.clear
    ARGV << "remove"
    ARGV << "Sample.Files.git"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run

    ARGV.clear
    ARGV << "add"
    ARGV << "https://github.com/lou-parslow/Sample.Files.git"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run

    ARGV.clear
    ARGV << "remove"
    ARGV << "Sample.Files.git"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run
  end

  it "shows existing urls" do
    ARGV.clear
    ARGV << "show"
    ARGV << "raykit"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run
  end

  it "will rake a working copy" do
    ARGV.clear
    ARGV << "work"
    ARGV << "https://github.com/lou-parslow/Sample.Files.git"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run

    ARGV.clear
    ARGV << "work"
    ARGV << "https://github.com/lou-parslow/Sample.Files.git"
    ARGV << "--task"
    ARGV << "clean"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run
  end

  # it "can import urls from DEV_ROOT" do
  #    ARGV.clear
  #    ARGV << "import"
  #    ARGV << "lou-parslow"
  #    puts ''
  #    puts Rainbow("raykit " + ARGV.join(' ')).blue.bright
  #    console = Raykit::Console.new
  #    console.run
  # end

  it "will clean work directories" do
    ARGV.clear
    ARGV << "work"
    ARGV << "https://github.com/lou-parslow/Sample.Files.git"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run

    ARGV.clear
    ARGV << "clean"
    ARGV << "Sample.Files.git"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run
  end

  it "will perform a git pull on existing work directories" do
    ARGV.clear
    ARGV << "pull"
    ARGV << "lou-parslow"
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run
  end
end

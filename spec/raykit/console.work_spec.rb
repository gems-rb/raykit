# frozen_string_literal: true

require_relative "../../lib/raykit"

describe Raykit::Console do
  it "work by raking in local directory" do
    ARGV.clear
    ARGV << "work"
    ARGV << "elements"
    # ARGV << '--verbose'
    puts ""
    puts Rainbow("raykit #{ARGV.join(" ")}").blue.bright
    console = Raykit::Console.new
    console.run
  end
end

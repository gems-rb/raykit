require "./lib/raykit"

#LATEST_TAG = GIT_DIRECTORY.latest_tag "main"
#BRANCH = GIT_DIRECTORY.branch

#PRIMARY_ARTIFACT = "#{PROJECT.name}-#{PROJECT.version}.gem"

#if (File.exist?(PRIMARY_ARTIFACT))
#  puts "PRIMARY_ARTIFACT exists"
#  PRIMARY_ARTIFACTS_LAST_MODIFIED = File.mtime(PRIMARY_ARTIFACT)
#  puts "PRIMARY_ARTIFACTS_LAST_MODIFIED #{PRIMARY_ARTIFACTS_LAST_MODIFIED}"
#else
#  puts "PRIMARY_ARTIFACT does not exist"
#end

#PROJECT.name = NAME
#PROJECT.version = VERSION
PROJECT.description = "ruby gem to support rake ci/cd tasks"

#puts "PROJECT INFO"
#puts "  #{PROJECT.name} v#{PROJECT.version} (#{PROJECT.description})"

#FileUtils.mkdir("tmp") if (!Dir.exist?("tmp"))
#run("ruby --version").log_to_file("tmp/ruby.version.txt")
#make_log("tmp/ruby.version.txt", "ruby --version")

desc "build the gem"
task :build do
  run("bundle")
  run("gem build raykit.gemspec")
end

desc "test the gem"
task :test => [:build] do
  #start_task :test
  run("ruby -r minitest/autorun test/test_raykit.rb --verbose")
  buildInfo = Raykit::Conan::BuildInfo.new("test/raykit/conan/conanbuildinfo.txt")
end

task :integrate => [:test] do
  start_task :integrate
  if !PROJECT.read_only?
    Rake::Task["test"].invoke if PROJECT.outstanding_commit? && Rake::Task.task_defined?("test")
    if PROJECT.latest_tag != PROJECT.version
      gem = "#{PROJECT.name}-#{PROJECT.version}.gem"
      if !File.exist?(gem)
        puts "gem file #{gem} does not exist"
      else
        #if GIT_DIRECTORY.has_tag PROJECT.version
        #  puts "tag #{PROJECT.version} already exists"
        #else
        #  md5 = Digest::MD5.file gem
        #  commit_message = "tag=#{PROJECT.version},md5=#{md5}"
        #  run("git commit -m\"#{commit_message}\"") if PROJECT.outstanding_commit?
        #  run("git tag #{PROJECT.version} -m\"#{commit_message}\"")
        #  run("git push --tags", false)
        #end
      end
    end
  end
end

task :publish => [:test, :integrate] do
  start_task :publish
  gem = "#{PROJECT.name}-#{PROJECT.version}.gem"
  puts "gem file #{gem} does not exist" unless File.exist?(gem)

  gem_list_text = Raykit::Command::new("gem list --remote raykit").run.output
  if gem_list_text.include?("raykit")
    run("gem list --remote raykit")
    latest_published_version = `gem list --remote raykit`.gsub(" ruby", "").scan(/raykit \(([\d.]+)\)/).last.first
    all_versions = `gem list --remote raykit --all`
    puts "  last published version #{latest_published_version}"
    if PROJECT.version == latest_published_version || all_versions.include?("#{PROJECT.version}")
      puts "  gem #{gem} already published"
    else
      run("gem push #{gem}", true)
    end
  else
    puts "  unable to list raykit from --remote, internet may not be available"
  end
end

# :publish task may be run manually, but by default publish will be handled by the CI/CD pipeline
task :default => [:integrate, :tag, :publish] do
  puts "Runtime identifier: #{Raykit::DotNet::get_runtime_identifier()}"
  puts "Runtime identifier: #{RUNTIME_IDENTIFIER}"
  if (!PROJECT.read_only?)
    try("rufo .")
    #run("git integrate")
    try("git pull")
    try("git push")
  end
  FileUtils.mkdir("artifacts") if (!Dir.exist?("artifacts"))
  `git log -n 1 > artifacts/latest_commit.txt`
  PROJECT.summary
  #File.write("rake.md", PROJECT.to_markdown)
end

desc "bump the version"
task :bump do
  start_task :bump
  run("gem build raykit.gemspec")
  old_version = Raykit::Version.detect(PROJECT.name)
  new_version = Raykit::Version.bump_file("raykit.gemspec")
  puts "  bumped version from #{old_version} to #{new_version}"
end

task :test_setup do
  GIT_DIRECTORY.setup
end

task :test_gitignore_content do
  puts "#{Raykit::DefaultContent::gitignore}"
end

task :test_initialize_razorclasslib do
  Raykit::DotNet::initialize_csharp_razorclasslib "MyRazorComponents"
  Raykit::DotNet::initialize_csharp_blazorserver("MyBlazorServer")
end

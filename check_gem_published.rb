require "net/http"
require "json"

gem_name = ARGV[0]
gem_version = ARGV[1]

url = URI("https://rubygems.org/api/v1/versions/#{gem_name}.json")
response = Net::HTTP.get(url)
versions = JSON.parse(response).map { |v| v["number"] }

if versions.include?(gem_version)
  puts "Version #{gem_version} of #{gem_name} is already published."
  exit 1
else
  puts "Version #{gem_version} of #{gem_name} is not published."
  exit 0
end

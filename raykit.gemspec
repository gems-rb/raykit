Gem::Specification.new do |gem|
  gem.name = "raykit"
  gem.version = "0.0.548"
  gem.summary = "ruby gem to support rake ci/cd tasks"
  gem.description = "supports common ci/cd development tasks"
  gem.authors = ["Lou Parslow"]
  gem.email = "lou.parslow@gmail.com"
  gem.homepage = "http://rubygems.org/gems/raykit"
  gem.required_ruby_version = ">= 2.0.0"
  gem.files = Dir["LICENSE", "README.md", "{lib}/**/*.{rb,txt}"]
  gem.executables = ["raykit"]
  gem.licenses = ["MIT"]
  gem.add_runtime_dependency "bundler", "~>2.2"
  gem.add_runtime_dependency "rainbow", "~>3.0"
  gem.add_runtime_dependency "slop", "~>4.8"
  gem.add_runtime_dependency "yard", "~>0.9"
  gem.add_runtime_dependency "rubyzip", "~>2.3"
  gem.add_runtime_dependency "digest", "~>3.0"
  gem.add_runtime_dependency "etc", "~>1.2"
end

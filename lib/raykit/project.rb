# frozen_string_literal: true

require "rake/clean"
require "digest"

module Raykit
  class Project
    attr_accessor :name, :description, :version, :timer, :verbose, :target

    @directory
    #@version
    @remote
    @repository
    @git_directory
    @timeout
    @values
    @commands

    # @log
    # @commit_message_filename

    # TODO: refactor to remove the defaults
    def initialize(name = "", description = "", version = "")
      @description = description if description.length.positive?
      @version = version if version.length.positive?
      @timeout = 1000 * 60 * 15
      @verbose = false
      @timer = Raykit::Timer.new
      @remote = ""
      @commit_message_filename = "commit_message.tmp"
      if Dir.exist?(RAKE_DIRECTORY)
        @directory = RAKE_DIRECTORY
        if Dir.exist?("#{RAKE_DIRECTORY}/.git") && Dir.exist?(@directory)
          @git_directory = Raykit::Git::Directory.new(@directory)
          @remote = @git_directory.remote
        end
      else
        @directory = ""
      end

      # @log=Log.new("#{RAKE_DIRECTORY}/tmp/raykit.log")
      if (name.length.positive?)
        @name = name
      else
        if defined?(NAME)
          @name = NAME
        else
          slns = Dir.glob("*.sln")
          if slns.length == 1
            @name = slns[0].gsub(".sln", "")
          else
            gemspecs = Dir.glob("*.gemspec")
            if gemspecs.length == 1
              @name = gemspecs[0].gsub(".gemspec", "")
            else
              remote_parts = @remote.split("/")
              @name = remote_parts[-1].gsub(".git", "") if remote_parts.length.positive?
            end
          end
        end
      end

      if version.length.zero?
        if defined?(VERSION)
          @version = VERSION
        else
          @version = Raykit::Version.detect(@name)
        end
      end

      if description.length.zero?
        @description = DESCRIPTION if defined?(DESCRIPTION)
      end

      if (@remote.nil? || @remote.length.zero?)
        @repository = nil
      else
        @repository = Raykit::Git::Repository.new(@remote)
      end
      @values = Hash::new()
      @commands = Array::new()
    end

    def get_dev_dir(subdir)
      Raykit::Environment.get_dev_dir(subdir)
    end

    attr_reader :log, :directory, :remote

    # def target
    #    @target
    # end
    def target_md5
      return Digest::MD5.file(target).to_s.strip if !target.nil? && File.exist?(target)

      ""
    end

    def branch
      return "main" if @git_directory.nil?
      @git_directory.branch
    end

    def values
      @values
    end

    # latest local commit
    def latest_commit
      return "" if detached?
      Dir.chdir(@directory) do
        text = `git log -n 1`
        scan = text.scan(/commit (\w+)\s/)
        return scan[0][0].to_s
      end
    end

    def outstanding_commit?
      Dir.chdir(@directory) do
        return true unless `git status`.include?("nothing to commit")
      end
      false
    end

    def outstanding_tag?
      # puts `git log -n 1`
      # !latest_tag_commit.eql?(latest_commit)
      # commit 2e4cb6d6c0721e16cd06afee85b7cdc27354921b (HEAD -> master, tag: 0.0.180, origin/master, origin/HEAD)
      outstanding_commit? || !`git log -n 1`.include?("tag:")
    end

    def read_only?
      return true if !File.exist?(".git") || detached?
      return false
    end

    def detached?
      return true if @git_directory.nil?
      @git_directory.detached?
    end

    def has_diff?(filename)
      Dir.chdir(@directory) do
        text = `git diff #{filename}`.strip
        return true if text.length.positive?
      end
    end

    def set_version(version)
      @version = version
    end

    def latest_tag
      `git describe  --abbrev=0`.strip
    end

    def latest_tag_commit
      `git show-ref -s #{latest_tag}`.strip
    end

    def latest_tag_md5
      text = `git tag #{latest_tag} -n3`
      scan = text.scan(/md5=(\w{32})/)
      return scan[0][0].to_s.strip if scan.length.positive? && scan[0].length.positive?

      ""
    end

    def last_modified_filename
      Dir.chdir(@directory) do
        Dir.glob("**/*.*").select { |f| File.file?(f) }.max_by { |f| File.mtime(f) }
      end
    end

    def size
      Dir.chdir(@directory) do
        text = `git count-objects -v -H`
        if matches = text.match(/size: ([. \w]+)$/)
          matches[1]
        else
          text
        end
      end
    end

    def size_pack
      Dir.chdir(@directory) do
        text = `git count-objects -v -H`
        if matches = text.match(/size-pack: ([. \w]+)$/)
          matches[1]
        else
          text
        end
      end
    end

    def elapsed
      elapsed = @timer.elapsed
      if elapsed < 1.0
        "#{format("%.1f", @timer.elapsed)}s"
      else
        "#{format("%.0f", @timer.elapsed)}s"
      end
    end

    def info
      ljust = 35
      puts ""
      puts "PROJECT.name".ljust(ljust) + Rainbow(PROJECT.name).yellow.bright
      puts "PROJECT.version".ljust(ljust) + Rainbow(PROJECT.version).yellow.bright
      puts "PROJECT.remote".ljust(ljust) + Rainbow(PROJECT.remote).yellow.bright
      puts "PROJECT.branch".ljust(ljust) + Rainbow(PROJECT.branch).yellow.bright
      puts "PROJECT.detached?".ljust(ljust) + Rainbow(PROJECT.detached?).yellow.bright
      # puts "PROJECT.target".ljust(ljust) + Rainbow(PROJECT.target).yellow.bright
      # puts "PROJECT.target_md5".ljust(ljust) + Rainbow(PROJECT.target_md5).yellow.bright
      puts "PROJECT.latest_tag".ljust(ljust) + Rainbow(PROJECT.latest_tag).yellow.bright
      puts "PROJECT.latest_tag_commit".ljust(ljust) + Rainbow(PROJECT.latest_tag_commit).yellow.bright
      puts "PROJECT.latest_tag_md5".ljust(ljust) + Rainbow(PROJECT.latest_tag_md5).yellow.bright
      puts "PROJECT.latest_commit".ljust(ljust) + Rainbow(PROJECT.latest_commit).yellow.bright
      puts "PROJECT.last_modified_filename".ljust(ljust) + Rainbow(PROJECT.last_modified_filename).yellow.bright
      # puts "PROJECT.elapsed".ljust(ljust) + Rainbow(PROJECT.elapsed).yellow.bright
      puts "PROJECT.size".ljust(ljust) + Rainbow(PROJECT.size).yellow.bright
      puts "PROJECT.size_pack".ljust(ljust) + Rainbow(PROJECT.size_pack).yellow.bright
      puts "PROJECT.outstanding_commit?".ljust(ljust) + Rainbow(PROJECT.outstanding_commit?).yellow.bright
      puts "PROJECT.outstanding_tag?".ljust(ljust) + Rainbow(PROJECT.outstanding_tag?).yellow.bright
      puts ""
      self
    end

    def to_markdown()
      md = "# #{@name}"
      md += get_markdown_nvp("Name", "Value", " ")
      md += get_markdown_nvp("-", "-", "-")
      md += get_markdown_nvp("Version", "#{@version}", " ")
      md += get_markdown_nvp("Machine", "#{Raykit::Environment::machine}", " ")
      md += get_markdown_nvp("User", "#{Raykit::Environment::user}", " ")
      @values.each do |key, value|
        md += get_markdown_nvp(key, value, " ")
      end
      @commands.each do |cmd|
        md = md + "\n" + cmd.to_markdown
      end
      md
    end

    def get_markdown_nvp(key, value, pad_char)
      "\n| " + key.to_s.ljust(36, pad_char) + "| " + value.to_s.ljust(36, pad_char)
    end

    def summary
      info if @verbose
      puts "[#{elapsed}] #{Rainbow(@name).yellow.bright} #{Rainbow(version).yellow.bright}"
    end

    def commit_message_filename
      warn "[DEPRECATION] 'commit_message_filename' is deprecated."
      @commit_message_filename
    end

    def commit(commit_message)
      warn "[DEPRECATION] 'commit_message_filename' is deprecated. Use a run command for better transparency."
      Dir.chdir(@directory) do
        if File.exist?(".gitignore")
          status = `git status`
          if status.include?("Changes not staged for commit:")
            run("git add --all")
            if GIT_DIRECTORY.outstanding_commit?
              if File.exist?(@commit_message_filename)
                run("git commit --file #{@commit_message_filename}")
                File.delete(@commit_message_filename)
              else
                run("git commit -m'#{commit_message}'")
              end
            end
          end
        else
          puts "warning: .gitignore not found."
        end
      end
      self
    end

    def push
      Dir.chdir(@directory) do
        status = `git status`
        if status.include?('use "git push"')
          run("git push")
        end
      end
      self
    end

    def pull
      Dir.chdir(@directory) do
        run("git pull")
      end
      self
    end

    def tag
      warn "[DEPRECATION] 'tag' is deprecated. Use run command(s) for better transparency."
      Dir.chdir(@directory) do
        specific_tag = `git tag -l #{@version}`.strip
        puts Rainbow("git tag - #{@version}").green if @verbose
        puts `git tag -l #{@version}` if @verbose
        if specific_tag.length.zero?
          puts "tag #{@version} not detected." if @verbose
          tag = `git describe --abbrev=0 --tags`.strip
          if @version != tag
            puts "latest tag #{@tag}" if @verbose
            run("git tag #{@version} -m'#{@version}'")
            run("git push origin #{@version}")
            # push
          end
        elsif @verbose
          puts "already has tag #{specific_tag}"
        end
      end
      self
    end

    def run(command, quit_on_failure = true)
      if command.is_a?(Array)
        command.each { |subcommand| run(subcommand, quit_on_failure) }
      else
        cmd = Command.new(command).set_timeout(@timeout).run
        cmd.summary
        elapsed_str = Timer.get_elapsed_str(cmd.elapsed, 0)
        if !cmd.exitstatus.nil? && cmd.exitstatus.zero?
        else
          # display error details
          cmd.details
          if quit_on_failure
            abort
          end
        end
        cmd.save
        @commands << cmd
        cmd
      end
    end
  end
end

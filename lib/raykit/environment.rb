# frozen_string_literal: true

require "pathname"

module Raykit
  # Provides functionality related to the development environment
  class Environment
    def self.info
      ljust = 35
      puts "machine".ljust(ljust) + Rainbow("#{machine}").yellow.bright
      puts "user".ljust(ljust) + Rainbow("#{user}").yellow.bright
      puts "root_dir".ljust(ljust) + Rainbow("#{root_dir}").yellow.bright
      puts "home_dir".ljust(ljust) + Rainbow("#{home_dir}").yellow.bright
      puts "bin_dir".ljust(ljust) + Rainbow("#{bin_dir}").yellow.bright
      puts "log_dir".ljust(ljust) + Rainbow("#{log_dir}").yellow.bright
      puts "local_application_data".ljust(ljust) + Rainbow("#{local_application_data}").yellow.bright
      puts "admin".ljust(ljust) + Rainbow("#{admin?}").yellow.bright
      self
    end

    # Normalize a directory or filename to use forward slashes
    def self.normalize_path(name)
      if (windows?)
        normalize_windows(name)
      else
        normalize_unix(name)
      end
    end

    def self.normalize_unix(name)
      name.gsub('\\', "/")
    end

    def self.normalize_windows(name)
      name.gsub("/", '\\')
    end

    def self.windows?
      Gem.win_platform?
    end

    # The root directory for the development environment.
    # May be set using the environment variable DEV_ROOT,
    # otherwise defaults to the user home directory
    def self.root_dir
      if ENV["DEV_ROOT"].nil?
        config = Raykit::Configuration.new
        if (config.root_dir.nil? || config.root_dir.empty?)
          root = Environment.home_dir + File::SEPARATOR + "code"
        else
          root = Environment.home_dir + File::SEPARATOR + config.root_dir
        end
        normalize_path(root)
      else
        root = ENV["DEV_ROOT"].gsub("\\", "/").chomp("/")
        normalize_path(root)
      end
    end

    # The user home directory
    def self.home_dir
      return normalize_path(ENV["USERPROFILE"]) if ENV.include?("USERPROFILE")
      normalize_path(ENV["HOME"])
    end

    def self.log_dir
      get_dev_dir("log")
    end

    def self.bin_dir
      get_dev_dir("bin")
    end

    # Get, and create if it does not exist, a specific development directory
    def self.get_dev_dir(name)
      dir = Pathname.new("#{Environment.root_dir}/#{name}")
      dir.mkpath
      if (dir.to_s.include?("https:") || dir.to_s.include?("http:"))
        normalize_path(dir.to_s.gsub("https://", ".").gsub("http://", ".").gsub("//", "/"))
      else
        normalize_path(dir.to_s)
      end
    end

    def self.get_work_dir(url)
      "#{Raykit::Environment.get_dev_dir("work")}/#{url.gsub(".git", "").gsub("://", ".")}"
    end

    # Get the size of a directory and its contents
    def self.get_dir_size(dir)
      Dir.glob(File.join(dir, "**", "*"))
         .map { |f| File.size(f) }
         .inject(:+)
    end

    def self.machine
      return ENV["COMPUTERNAME"] unless ENV["COMPUTERNAME"].nil?

      machine = `hostname`
      machine = machine.split(".")[0] if machine.include?(".")
      machine.strip
    end

    def self.user
      ENV["USERNAME"]
    end

    def self.local_application_data
      normalize_path("#{ENV["USERPROFILE"]}/AppData/Local".gsub('\\', "/"))
    end

    def self.admin?
      rights = `whoami /priv`
      rights.include?("SeCreateGlobalPrivilege")
    end

    def self.which(name)
      return name if File.exist?(name)

      ["", ".exe", ".bat", ".cmd"].each do |ext|
        aname = name + ext
        return aname if File.exist?(aname)

        ENV["PATH"].split(File::PATH_SEPARATOR).each do |path|
          apath = "#{path.gsub('\\', "/")}/#{aname}".gsub("//", "/")
          return apath if File.exist?(apath)
        end
      end
      ""
    end
  end
end

# frozen_string_literal: true

module Raykit
  class ConanPackage
    attr_accessor :name, :version

    def initialize(name, version)
      @name = name
      @version = version
    end

    def self.get_version(text, name)
      # "gtest/0.0.14@my+channel.com/branch"
      # gtest/1.10.0
      [/\b#{name}\/([\d\.]+)/].each { |regex|
        matches = text.scan(regex)
        if matches.length > 0 && matches[0].length > 0
          return matches[0][0]
        end
      }
      ""
    end

    def self.set_version(text, name, version)
      #puts "ConanPackage::set_version"
      new_text = text
      [/(\b#{name}\/[\d\.]+)/].each { |regex|
        matches = text.scan(regex)
        if matches.length > 0 && matches[0].length > 0
          orig = matches[0][0]
          oversion = get_version(orig, name)
          mod = orig.sub(oversion, version)
          #puts "match[0][0] " + matches[0][0]
          #puts "old version " + oversion
          #puts "new version " + version
          #puts "updating from #{orig} to #{mod}"
          new_text = new_text.gsub(orig, mod)
        end
      }
      new_text
    end

    def self.set_version_in_file(filename, name, version)
      text = set_version(IO.read(filename), name, version)
      orig = IO.read(filename)
      File.open(filename, "w") { |f| f.puts text } if (text != orig)
    end
  end
end

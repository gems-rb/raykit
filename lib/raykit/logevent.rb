# frozen_string_literal: true

require "json"

module Raykit
  # :verbose, :debug, :information, :error, :fatal
  class LogEvent < Hash
    def initialize(level, messageTemplate, properties)
      self["Timestamp"] = DateTime.now.iso8601
      self["Level"] = level
      self["Message"] = messageTemplate
      self["MessageTemplate"] = messageTemplate
      properties["MachineName"] = Raykit::Environment.machine unless properties.key?("MachineName")
      properties["UserName"] = Raykit::Environment.user unless properties.key?("UserName")
      properties["RakeDirectory"] = ::Rake.application.original_dir
      self["Properties"] = properties
    end

    def to_seq
      unless ENV["SEQ_SERVER"].nil?
        cmd_str = "seqcli log -m \"#{self["Message"].gsub('"', "")}\" -l #{self["Level"]} -s #{ENV["SEQ_SERVER"]}"
        self["Properties"].each do |k, v|
          cmd_str += " -p \"#{k}=#{v}\""
        end
        puts `#{cmd_str}`
      end
    end
  end
end

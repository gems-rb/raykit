require "json"

module Raykit
  class Log < Hash
    @filename

    def initialize(filename)
      @filename = filename
      dir = File.dirname(@filename)
      FileUtils.mkdir_p(dir) unless Dir.exist?(dir)
      if File.exist?(@filename)
        begin
          data = JSON.parse(File.read(filename))
          data.each do |k, v|
            self[k] = v
          end
        rescue StandardError
        end
      end
    end

    def save
      File.open(@filename, "w") { |f| f.write(JSON.generate(self)) }
    end

    def update_command_time(command, timestamp)
      command_times = {}
      command_times = self["command_times"] if key?("command_times")
      command_times.delete(command) if command_times.key?(command)
      command_times[command] = timestamp.iso8601
      self["command_times"] = command_times
      save
    end

    def get_command_time(command)
      if key?("command_times")
        command_times = self["command_times"]
        return DateTime.parse(command_times[command]) if command_times.key?(command)
      end
      nil
    end

    def self.start_task(task_name)
      puts Rainbow(": #{task_name}").blue.bright
      MARKDOWN.puts(": #{task_name}")
    end

    def self.show_value(name, value)
      puts "  " + Rainbow(name).cyan + " " + Rainbow("#{value}").white.bold
      MARKDOWN.puts("#{name} #{value}")
    end

    def self.name_value_pair(symbol, the_binding)
      [symbol.to_s, eval(symbol.to_s, the_binding)]
    end

    def self.show_table(symbols)
      max_name_width = 0
      max_value_width = 0
      symbols.each { |s|
        nvp = name_value_pair(s, binding)
        name = nvp[0]
        value = nvp[1]
        max_name_width = name.length if (name.length > max_name_width)
        if (!value.nil?)
          max_value_width = value.length if (value.length > max_value_width)
        end
      }
      header = "  =".ljust(max_name_width + max_value_width + 5, "=")
      puts header
      symbols.each { |s|
        nvp = name_value_pair(s, binding)
        name = nvp[0].rjust(max_name_width, " ")
        value = nvp[1]
        puts "  " + Rainbow(name).cyan + " | " + Rainbow("#{value}").white.bold
      }
      puts header
    end
  end
end

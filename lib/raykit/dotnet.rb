# frozen_string_literal: true

module Raykit
  class DotNet
    def self.init_new(name, template)
      if (!File.exist?("#{name}/#{name}.csproj"))
        puts "  #{name}.csproj not found, creating..."
        run("dotnet new #{template} --name #{name} --output #{name} --language C# ")
      else
        puts "  #{name}.csproj already exists"
      end
    end

    # initialize an F# library
    def self.initialize_fsharp_lib(name)
      puts `dotnet new sln --name #{name}` unless File.exist?("#{name}.sln")
      unless Dir.exist?(name)
        FileUtils.mkdir(name)
        Dir.chdir(name) do
          puts `dotnet new classlib -lang F#`
        end
        # puts `dotnet new sln`
        puts `dotnet sln #{name}.sln add #{name}/#{name}.fsproj`

        FileUtils.mkdir("#{name}.Test") unless Dir.exist?("#{name}.Test")
        Dir.chdir("#{name}.Test") do
          puts `dotnet new nunit -lang F#`
          puts `dotnet add reference ../#{name}/#{name}.fsproj`
        end

        puts `dotnet sln #{name}.sln add #{name}.Test/#{name}.Test.fsproj`
      end
    end

    # initialize a C# console application
    def self.initialize_csharp_console(name)
      unless Dir.exist?(name)
        FileUtils.mkdir(name)
        Dir.chdir(name) do
          puts `dotnet new console -lang C#`
        end
        puts `dotnet new sln`
        puts `dotnet sln #{name}.sln add #{name}/#{name}.csproj`

        FileUtils.mkdir("#{name}.Test") unless Dir.exist?("#{name}.Test")
        Dir.chdir("#{name}.Test") do
          puts `dotnet new nunit -lang C#`
          puts `dotnet add reference ../#{name}/#{name}.csproj`
        end

        puts `dotnet sln #{name}.sln add #{name}.Test/#{name}.Test.csproj`
      end
    end

    # initialize a C# wpf application
    def self.initialize_csharp_wpf_application(name)
      puts `dotnet new sln --name #{name}`
      unless Dir.exist?(name)
        FileUtils.mkdir(name)
        Dir.chdir(name) do
          puts `dotnet new wpf -lang C#`
        end
      end
      puts `dotnet sln #{name}.sln add #{name}/#{name}.csproj`
      initialize_csharp_lib("#{name}.Library")
      puts `dotnet sln #{name}.sln add #{name}.Library/#{name}.Library.csproj`
      puts `dotnet sln #{name}.sln add #{name}.Library.Test/#{name}.Library.Test.csproj`
    end

    # initialize a C# worker service
    def self.initialize_csharp_service(name)
      unless Dir.exist?(name)
        FileUtils.mkdir(name)
        Dir.chdir(name) do
          puts `dotnet new worker -lang C#`
        end
        puts `dotnet new sln`
        puts `dotnet sln #{name}.sln add #{name}/#{name}.csproj`
      end
    end

    def self.initialize_csharp_project(template_name, dir, name, folders)
      puts "  creating #{dir}/#{name}/#{name}.csproj" unless File.exist?("#{dir}/#{name}/#{name}.csproj")
      PROJECT.run("dotnet new #{template_name} -lang C# --name #{name} --output #{dir}/#{name}") unless File.exist?("#{dir}/#{name}/#{name}.csproj")
      folders.each do |folder|
        FileUtils.mkdir_p("#{dir}/#{name}/#{folder}") unless Dir.exist?("#{dir}/#{name}/#{folder}")
      end
    end

    # initialize a C# library
    def self.initialize_csharp_lib(name)
      # create the library
      puts "  creating src/#{name}/#{name}.csproj" unless File.exist?("src/#{name}/#{name}.csproj")
      PROJECT.run("dotnet new classlib -lang C# --name #{name} --output src/#{name}") unless File.exist?("src/#{name}/#{name}.csproj")
      # create the test
      puts "  creating test/#{name}.Test/#{name}.Test.csproj" unless File.exist?("test/#{name}.Test/#{name}.Test.csproj")
      PROJECT.run("dotnet new nunit -lang C# --name #{name}.Test --output test/#{name}.Test") unless File.exist?("test/#{name}.Test/#{name}.Test.csproj")
      PROJECT.run("dotnet add test/#{name}.Test/#{name}.Test.csproj reference src/#{name}/#{name}.csproj") unless File.exist?("test/#{name}.Test/#{name}.Test.csproj")
      # generate project folders
      ["Interfaces", "Extensions", "Models"].each do |folder|
        FileUtils.mkdir_p("src/#{name}/#{folder}") unless Dir.exist?("src/#{name}/#{folder}")
        FileUtils.mkdir_p("test/#{name}.Test/#{folder}") unless Dir.exist?("test/#{name}.Test/#{folder}")
      end
    end

    # initialize a C# razor class library
    def self.initialize_csharp_razorclasslib(name)
      # create the library
      puts "  creating src/#{name}/#{name}.csproj" unless File.exist?("src/#{name}/#{name}.csproj")
      PROJECT.run("dotnet new razorclasslib -lang C# --name #{name} --output src/#{name}") unless File.exist?("src/#{name}/#{name}.csproj")
      # create the test
      puts "  creating test/#{name}.Test/#{name}.Test.csproj" unless File.exist?("test/#{name}.Test/#{name}.Test.csproj")
      PROJECT.run("dotnet new nunit -lang C# --name #{name}.Test --output test/#{name}.Test") unless File.exist?("test/#{name}.Test/#{name}.Test.csproj")
      PROJECT.run("dotnet add test/#{name}.Test/#{name}.Test.csproj reference src/#{name}/#{name}.csproj") unless File.exist?("test/#{name}.Test/#{name}.Test.csproj")
      # generate project folders
      ["Interfaces", "Extensions", "Models", "Components", "Controllers"].each do |folder|
        FileUtils.mkdir_p("src/#{name}/#{folder}") unless Dir.exist?("src/#{name}/#{folder}")
        FileUtils.mkdir_p("test/#{name}.Test/#{folder}") unless Dir.exist?("test/#{name}.Test/#{folder}")
      end
    end

    # initialize a C# blazor server application
    def self.initialize_csharp_blazorserver(name)
      # create the blazor server application
      puts "  creating src/#{name}/#{name}.csproj" unless File.exist?("src/#{name}/#{name}.csproj")
      PROJECT.run("dotnet new blazorserver -lang C# --name #{name} --output src/#{name}") unless File.exist?("src/#{name}/#{name}.csproj")
      # generate project folders
      ["Interfaces", "Extensions", "Models", "Components", "Controllers"].each do |folder|
        FileUtils.mkdir_p("src/#{name}/#{folder}") unless Dir.exist?("src/#{name}/#{folder}")
      end
    end

    # initialize a C# blazor server example application
    def self.initialize_csharp_blazorserver_example(name)
      # create the blazor server application
      puts "  creating examples/#{name}/#{name}.csproj" unless File.exist?("examples/#{name}/#{name}.csproj")
      PROJECT.run("dotnet new blazorserver -lang C# --name #{name} --output examples/#{name}") unless File.exist?("examples/#{name}/#{name}.csproj")
      # generate project folders
      ["Interfaces", "Extensions", "Models", "Components", "Controllers"].each do |folder|
        FileUtils.mkdir_p("examples/#{name}/#{folder}") unless Dir.exist?("examples/#{name}/#{folder}")
      end
    end

    def self.initialize_csharp_wpf_example(name)
      # create the wpf application
      puts "  creating examples/#{name}/#{name}.csproj" unless File.exist?("examples/#{name}/#{name}.csproj")
      PROJECT.run("dotnet new wpf -lang C# --name #{name} --output examples/#{name}") unless File.exist?("examples/#{name}/#{name}.csproj")
      # generate project folders
      ["Interfaces", "Extensions", "Models", "Components", "Controllers"].each do |folder|
        FileUtils.mkdir_p("examples/#{name}/#{folder}") unless Dir.exist?("examples/#{name}/#{folder}")
      end
    end

    # create a solution of the specified name and add all projects in the current directory
    def self.create_solution(name)
      PROJECT.run("dotnet new sln --name #{name}") unless File.exist?("#{name}.sln")
      Dir.glob("**/*.csproj").each do |project|
        PROJECT.run("dotnet sln #{name}.sln add #{project}")
      end
    end

    def self.get_package_names(filename)
      package_names = []
      if File.exist?(filename) && filename.include?(".csproj")
        regex = /<PackageReference Include="([\w.-]+)"/
        text = IO.read(filename)
        text.scan(regex).each do |m|
          package_names << m.first.to_s
        end
      end
      package_names
    end

    def self.get_runtime_identifier()
      os = RbConfig::CONFIG["host_os"]
      cpu = RbConfig::CONFIG["host_cpu"]

      case os
      when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
        os_rid = "win"
      when /darwin|mac os/
        os_rid = "osx"
      when /linux/
        os_rid = "linux"
      when /solaris|bsd/
        os_rid = "unix"
      else
        raise "Unknown operating system: host_os=#{os}"
      end

      case cpu
      when /x86_64|amd64|x64/
        arch_rid = "x64"
      when /i686|i386/
        arch_rid = "x86"
      when /arm/
        arch_rid = "arm"
      when /aarch64/
        arch_rid = "arm64"
      else
        raise "Unknown architecture: host_cpu=#{cpu}"
      end

      "#{os_rid}-#{arch_rid}"
    end
  end
end

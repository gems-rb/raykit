# https://symbl.cc/en/unicode/table/
module Raykit
  class Symbols
    def self.checkmark
      Rainbow("\u2713").green
    end
    def self.success
      Rainbow("\u2713").green
    end
    def self.error
      Rainbow("\u0058").red
    end
    def self.warning
      Rainbow("\u26A0").yellow
    end
    def self.debug
      Rainbow("\u24D8").blue  # Circle with "i"
    end
    def self.info
      Rainbow("\u2139").blue  # Information source
    end
    def self.fatal
      Rainbow("\u25A0").bright.red  # Filled square, indicating stop/critical
    end
    def self.warn
      Rainbow("\u26A0").yellow  # Warning sign
    end
    def self.star
      Rainbow("\u2605").yellow  # Star
    end
    def self.coffee
      Rainbow("\u2615").yellow  # Hot beverage
    end
    def self.heart
      Rainbow("\u2764").red  # Heavy black heart
    end
    def self.bug
      Rainbow("\u1F41E").yellow  # Bug
    end
    def self.gear
      Rainbow("\u2699").yellow  # Gear
    end
    def self.magnifying_glass
      Rainbow("\u1F50D").yellow  # Left-pointing magnifying glass
    end
    def self.question
      Rainbow("\u003F").yellow  # Question mark
    end
    def self.arrow_right
      Rainbow("\u27A4").yellow  # Arrow pointing rightwards then curving upwards
    end
    def self.arrow_left
      Rainbow("\u2B05").yellow  # Left arrow
    end
    def self.arrow_up
      Rainbow("\u2B06").yellow  # Up arrow
    end
    def self.arrow_down
      Rainbow("\u2B07").yellow  # Down arrow
    end
    def self.arrow_up_down
      Rainbow("\u2195").yellow  # Up/down arrow
    end
    def self.arrow_left_right
      Rainbow("\u2194").yellow  # Left/right arrow
    end
    def self.arrow_upper_right
      Rainbow("\u2197").yellow  # North east arrow
    end
    def self.arrow_lower_right
      Rainbow("\u2198").yellow  # South east arrow
    end
    def self.arrow_lower_left
      Rainbow("\u2199").yellow  # South west arrow
    end
    def self.arrow_upper_left
      Rainbow("\u2196").yellow  # North west arrow
    end
    def self.arrow_up_small
      Rainbow("\u2B06").yellow  # Up arrow
    end
    def self.arrow_down_small
      Rainbow("\u2B07").yellow  # Down arrow
    end
    def self.arrow_right_small
      Rainbow("\u27A1").yellow  # Black rightwards arrow
    end
    def self.arrow_left_small
      Rainbow("\u2B05").yellow  # Left arrow
    end
    def self.arrow_up_down_small
      Rainbow("\u2195").yellow  # Up/down arrow
    end

    def self.music
      Rainbow("\u266B").yellow  # Beamed eighth notes
    end
    def self.get_severity_symbol(severity)
      case severity
      when "DEBUG"
        question
        #when "INFO"
        #  info
      when "WARN"
        warning
      when "ERROR"
        error
      when "FATAL"
        fatal
      else
        " "
      end
    end # def get_severity_symbol
  end # class Symbols
end # module Raykit

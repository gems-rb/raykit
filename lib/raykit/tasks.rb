# frozen_string_literal: true

desc "Display project information"
task :info do
  PROJECT.info
end

desc "Display environment information"
task :env do
  Raykit::Environment.info
end

desc "Display the constants"
task :show_constants do
  ljust = 35
  GIT_DIRECTORY.get_unique_ruby_constants.each do |constant|
    if Object.const_defined?(constant)
      value = Object.const_get(constant) #"?"# GIT_DIRECTORY.const_get(constant)
      puts constant.ljust(ljust) + Rainbow(value).yellow.bright
    end
  end
end

desc "integrate changes into the git repository"
task :integrate do
  puts Rainbow(": integrate").blue.bright

  if PROJECT.read_only?
    puts "  read only state, skipping integrate operations"
  else
    git_dir = Raykit::Git::Directory.new(Rake.application.original_dir)
    if git_dir.detached?
      puts "  detached head state, skipping integrate operations"
    else
      if PROJECT.outstanding_commit?
        Rake::Task["test"].invoke if Rake::Task.task_defined?("test")
      else
        puts "  no outstanding commits detected"
      end

      if !File.exist?(".gitignore")
        puts "warning: .gitignore does not exist."
      else
        PROJECT.run("git add --all")
        unless `git status`.include?("nothing to commit")
          commit_message = "integrate"
          PROJECT.run("git commit -m\"#{commit_message}\"") if PROJECT.outstanding_commit?
        end
        PROJECT.run("git pull", false)
      end
    end
  end
end

desc "push changes including tags"
task :push do
  puts Rainbow(": push").blue.bright
  git_dir = Raykit::Git::Directory.new(Rake.application.original_dir)
  if git_dir.detached?
    puts "detached head state, skipping push operations"
  else
    PROJECT.run("git push") if (!PROJECT.read_only?)
    PROJECT.run("git push --tags") if (!PROJECT.read_only?)
  end
end

desc "clean files not tracked by git"
task :clean do
  puts Rainbow(": clean").blue.bright
  PROJECT.run("git clean -dXf", false)
end

desc "tag the git repository at the current version"
task :tag do
  puts Rainbow(": tag").blue.bright
  if PROJECT.read_only?
    puts "  read only state, skipping tag operation"
  else
    if (GIT_DIRECTORY.has_tag "v#{PROJECT.version}")
      puts "  tag #{PROJECT.latest_tag} already exists."
    else
      GIT_DIRECTORY.tag_version PROJECT.version
      try "git push --tags"
    end
  end
end

desc "update_source from sourceImports.json"
task :update_source do
  if File.exist?("sourceImports.json")
    puts Rainbow(":update_source").blue.bright
    sourceImports = Raykit::SourceImports.load("sourceImports.json")
    json = sourceImports.to_json
    sourceImports.update

    json2 = sourceImports.to_json
    if json2 != json || !sourceImports.targets_exist?
      sourceImports.save("sourceImports.json")
      sourceImports.copy
    else
      puts "        no update required."
    end
  end
end

desc "update source files"
task update: [:update_source]

desc "setup the project files and folders"
task :setup do
  start_task :setup
end
desc "build the project artifacts"
task :build do
  start_task :build
end

desc "test the project artifacts"
task :test do
  start_task :test
end

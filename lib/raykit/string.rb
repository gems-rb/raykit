# frozen_string_literal: true

require_relative("./environment")

class String
  def work_dir
    rel_name = sub("https://", "").gsub("http://", "").gsub(".git", "")
    "#{Raykit::Environment.get_dev_dir("work")}/#{rel_name}"
  end

  def latest_commit
    Dir.chdir(self) do
      text = `git log -n 1`
      scan = text.scan(/commit (\w+)\s/)
      return scan[0][0].to_s
    end
  end
end

# frozen_string_literal: true

module Raykit
  module Git
    # Functionality to manage a git commit
    class Commit
      attr_accessor :url, :branch, :commit_id

      def initialize(url, branch, commit_id)
        @url = url
        @branch = branch
        @commit_id = commit_id
      end
    end
  end
end

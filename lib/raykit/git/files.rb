# frozen_string_literal: true

module Raykit
  module Git
    # Functionality to manage a local clone of a git repository
    class Files
      @url
      @commit_id

      def initialize(url, commit_id)
        @url = url
        @commit_id = commit_id
      end

      def clean
        FileUtils.rm_rf(commit_path) if Dir.exist?(commit_path)
      end

      def get(name)
        puts "commit_path(): #{commit_path}"
        unless Dir.exist?(commit_path)
          puts "cloning commit path..."
          clone = Raykit::Command.new("git clone #{@url} #{commit_path}")
          puts clone.output
          puts clone.error
          Dir.chdir(commit_path) do
            checkout = Raykit::Command.new("git checkout #{@commit_id}")
          end
        end

        return filename(name) if File.exist?(filename(name))

        ""
      end

      def commit_path
        Dir.tmpdir + File::SEPARATOR + "Raykit.Git.Files" + File::SEPARATOR + @url.gsub("://",
                                                                                        ".") + File::SEPARATOR + @commit_id
      end

      def filename(name)
        commit_path + File::SEPARATOR + name
      end
    end
  end
end

# frozen_string_literal: true

module Raykit
  module Git
    # Functionality to manage a local clone of a git repository
    class Directory
      # The directory name of the local repository clone
      attr_accessor :directory

      @repository

      def initialize(directory)
        @directory = directory
      end

      def outstanding_commit?
        Dir.chdir(directory) do
          return false if !File.exist?(".git")
          if user_can_commit
            return !`git status`.include?("nothing to commit,")
          else
            return false
          end
        end
      end

      def detached?
        Dir.chdir(directory) do
          status = `git status`.strip
          if status.include?("detached")
            true
          else
            false
          end
        end
      end

      def pull
        Dir.chdir(directory) do
          diff = `git diff`.strip
          status = `git status`.strip
          PROJECT.run("git pull", false) if diff.length.zero? && status.include?("nothing to commit")
        end
      end

      def rake(task)
        unless Dir.exist?(@directory)
          puts "directory not found."
          return 1
        end
        debug = false
        sub_dir = "work"
        sub_dir = "make" if @directory.include?("/make/")
        rake_log = repository.get_dev_dir("log") + "/#{sub_dir}.rake.#{task}.json"

        puts "log filename #{rake_log}" if debug
        if File.exist?(rake_log) && (File.mtime(rake_log) > last_modified_time)
          puts "using logged data" if debug
          cmd = Raykit::Command.parse(File.read(rake_log))
          cmd.summary
          return
        end

        Dir.chdir(@directory) do
          if File.exist?("rakefile.rb")
            cmd = Raykit::Command.new("rake #{task}")
            cmd.summary
            FileUtils.mkdir_p(File.dirname(rake_log))
            File.open(rake_log, "w") { |f| f.write(JSON.generate(cmd.to_hash)) }
          else
            puts "rakefile.rb not found"
          end
        end
      end

      def last_modified_time
        Dir.chdir(@directory) do
          begin
            # Find the most recently modified file
            most_recent_file = Dir.glob("**/*.*").select { |f| File.file?(f) }.max_by { |f| File.mtime(f) }

            # Use File.mtime if a file was found, otherwise handle the nil case
            if most_recent_file
              last_modified_time = File.mtime(most_recent_file)
            else
              # Handle the case where no files are found. This could be setting a default value,
              # raising a custom error, or any other appropriate action depending on your needs.
              # For example, setting last_modified_time to Time.now or raising a custom error.
              last_modified_time = Time.now # Or any other appropriate action
            end
          rescue Errno::ENAMETOOLONG => e
            puts "Error: #{e.message}"
            last_modified_time = Time.now # Or any other appropriate action
          end

          #File.mtime(Dir.glob("**/*.*").select { |f| File.file?(f) }.max_by { |f| File.mtime(f) })
        end
      end

      def last_modified_filename
        Dir.chdir(@directory) do
          # TODO: consider handling the case where glob can fail because of filename length
          #       Errno::ENAMETOOLONG: File name too long @ dir_s_glob - /Users/username/...
          #
          begin
            # Use Dir.glob to find all files, select the actual files, find the one with the latest modification time, and return its name
            files = Dir.glob("**/*.*").select { |f| File.file?(f) }

            # Find the file with the latest modification time
            latest_file = files.max_by { |f| File.mtime(f) }

            return latest_file

            #Dir.glob("**/*.*").select { |f| File.file?(f) }.max_by { |f| File.mtime(f) }
          rescue Errno::ENAMETOOLONG => e
            puts "Error: #{e.message}"
            # return the first src file found
            src_files.each { |f| return f if File.file?(f) }
            #return handle_long_filename_error
          end
        end
      end

      # git ls-tree -r master --name-only
      def src_files
        files = Array.new
        Dir.chdir(@directory) do
          `git ls-tree -r #{branch} --name-only`.each_line { |line|
            file = line.strip
            files << file if file.length > 0 && File.exist?(file)
          }
        end
        files
      end

      def last_modified_src_time
        Dir.chdir(@directory) do
          last_file = self.src_files.select { |f| File.file?(f) }.max_by { |f| File.mtime(f) }
          last_file ? File.mtime(last_file) : Time.at(0) # Return a default time if no file is found
        end
      end

      #def last_modified_src_time
      #  #File.mtim(src_files.max_by { |f| File.mtime(f) }#
      #  Dir.chdir(@directory) do
      #    File.mtime(self.src_files.select { |f| File.file?(f) }.max_by { |f| File.mtime(f) })
      #  end
      #end

      def last_modified_src_file
        Dir.chdir(@directory) do
          # Select the source files that are actual files, not directories
          src_files.select { |f| File.file?(f) }
          # Find the max by modification time
                   .max_by { |f| File.mtime(f) }
        end
      end

      def last_modified_artifact_time
        self.last_modified_time
      end

      # The latest tag for a branch of the repository
      def latest_tag(_branch)
        Dir.chdir(directory) do
          return `git describe --abbrev=0`.strip
        end
        ""
      end

      def get_tag_commit_id(name)
        cmd = Raykit::Command.new("git rev-parse \"#{name}\"").run
        if !cmd.output.include?("fatal") && !cmd.error.include?("fatal")
          cmd.output.strip
        else
          ""
        end
      end

      def has_tag(name)
        cmd = Raykit::Command.new("git rev-parse \"#{name}\"").run
        if !cmd.output.include?("fatal") && !cmd.error.include?("fatal")
          true
        else
          false
        end
      end

      def get_sha(filename)
        if File.exist?(filename)
          Digest::SHA256.file(filename).hexdigest.to_s
        else
          "#{filename} not found"
        end
        # ''
      end

      def user_name
        `git config --get user.name`.strip
      end

      def user_email
        `git config --get user.email`.strip
      end

      def user_can_commit
        return false if user_name.length.zero?
        return false if user_email.length.zero?

        true
      end

      def branch
        Dir.chdir(directory) do
          scan = `git branch`.scan(/\*\s([\w.-]+)/)
          return scan[0][0].to_s if !scan.nil? && scan.length.positive? && scan[0].length.positive?
        end
        "master"
      end

      def repository
        @repository = Raykit::Git::Repository.new(remote) if @repository.nil?
        @repository
      end

      def remote
        if Dir.exist?(directory)
          Dir.chdir(directory) do
            return Command.new("git config --get remote.origin.url").run.output.strip if Dir.exist?(".git")
          end
        end
        ""
      end

      def tag_version(version)
        if has_tag "v#{version}"
          puts "  git tag v#{version} already exists"
        else
          if outstanding_commit?
            raise "outstanding commit, will not tag"
          else
            puts "  git tag v#{version} does not exist"
            run("git tag -a v#{version} -m\"version #{version}\"")
          end
        end
      end

      def get_unique_ruby_constants
        # Define the regular expression pattern
        pattern = /\b([A-Z][A-Z0-9_]*)\b\s*=/

        constants = []

        # Loop through each .rb file in the specified directory
        Dir.glob("#{directory}/**/*.rb").each do |file|
          # Read the contents of the file
          content = File.read(file)

          # Scan the content for constants and add the matches to the array.
          content.scan(pattern) do |match|
            # Add matched constant name to the constants array.
            constants << match[0]
          end
          # Scan the content for constants and add the matches to the array
          #constants.concat(content.scan(pattern))
        end

        # Return unique constants
        constants.uniq
      end

      #def setup
      #  DEFAULT_SUBDIRECTORIES.each do |subdirectory|
      #    FileUtils.mkdir_p(subdirectory)
      #  end
      #end
    end # class Directory
  end # module Git
end # module Raykit

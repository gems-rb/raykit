# frozen_string_literal: true

require_relative("./directory")

module Raykit
  module Git
    # Functionality to manage a remote git repository
    class Repositories < Array
      attr_accessor :filename

      def initialize(filename)
        @filename = filename
        open(@filename)
      end

      def open(filename)
        if File.exist?(filename)
          JSON.parse(File.read(filename)).each do |url|
            insert(-1, url)
          end
        else
          # puts "filename #{filename} does not exist"
        end
      end

      def save(_filename)
        File.open(@filename, "w") do |f|
          f.write(JSON.pretty_generate(self))
        end
      end

      def work_dir
        Environment.get_dev_dir("work")
      end

      def is_remote_url(pattern)
        return true if pattern.include?("http://")
        return true if pattern.include?("https://")

        false
      end

      def remove(url)
        if include?(url)
          delete(url)
          save(@filename)
        end
      end

      def import(pattern)
        imported = []
        if is_remote_url(pattern)
          remote = pattern
          unless include?(remote)
            insert(-1, remote)
            imported.insert(-1, remote)
          end
        else
          git_dirs = []
          Dir.chdir(work_dir) do
            Dir.glob("**/.git") do |git_dir|
              dir = File.expand_path("..", git_dir)
              git_dirs.insert(0, dir) if dir.length.positive?
            end
          end

          git_dirs.each do |git_dir|
            dir = Raykit::Git::Directory.new(git_dir)
            remote = dir.remote
            if remote.include?(pattern) && !include?(remote)
              insert(-1, remote)
              imported.insert(-1, remote)
            end
          end
        end
        save(@filename)
        imported
      end

      def matches(pattern)
        matches = []
        REPOSITORIES.each do |url|
          matches << url if url.include?(pattern)
        end
        matches
      end
    end
  end
end

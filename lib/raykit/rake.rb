# frozen_string_literal: true

require "time"

module Raykit
  class Rake
    def self.run(remote, branch, task = "default")
      repo = Raykit::Git::Repository.new(remote)
      rel_dir = repo.relative_path
      commit = repo.latest_commit(branch)
      log_filename = "#{Environment.get_dev_dir("log")}/RayKit.Runner/#{rel_dir}/#{branch}/#{commit}.json"
      if File.exist?(log_filename)
        Command.parse(File.read(log_filename))
      else
        run_dir = "#{Environment.get_dev_dir("tmp")}/#{rel_dir}.#{branch}.#{commit}"
        unless Dir.exist?(run_dir)
          parent_dir = File.expand_path("..", run_dir)
          FileUtils.mkdir_p(parent_dir) unless Dir.exist?(parent_dir)
          cmd = Command.new("git clone #{remote} #{run_dir}")
        end
        Dir.chdir(run_dir) do
          cmd = Command.new("rake #{task}")
          parent_dir = File.dirname(log_filename)
          FileUtils.mkdir_p(parent_dir) unless Dir.exist?(parent_dir)
          File.open(log_filename, "w") do |f|
            f.write(JSON.generate(cmd.to_hash))
          end
          return cmd
        end
      end
    end

    def self.rake(rakefile, task)
      Dir.chdir(File.dirname(rakefile)) do
        Raykit::TopLevel.run("rake #{task}", true)
      end
    end
  end
end

# frozen_string_literal: true

require "time"

module Raykit
  # Provides functionality to record the time execution times
  class Timer
    # The time at which start occurred
    attr_accessor :start_time

    def initialize
      @start_time = Time.now
    end

    # The elapsed time, in seconds, since the timer started
    def elapsed
      Time.now - @start_time
    end

    # The elapsed time, in seconds, as a formatted string
    def elapsed_str(pad = 0)
      Timer.get_elapsed_str(elapsed, pad)
    end

    # Converts a time span in seconds to a formatted string
    def self.get_elapsed_str(elapsed, pad = 0)
      # "[" + "%.0f" % (elapsed) + "s]".ljust(pad)
      format("%.0f", elapsed) + "s".ljust(pad)
    end
  end
end

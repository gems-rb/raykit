# frozen_string_literal: true

# warn "[DEPRECATION] Raykit::VersionHelper is deprecated. Use bump gem for better transparency."

module Raykit
  # Version functionality
  class Version
    attr_accessor :start_time

    # detect a version number based on the NAME variable, if defined
    def self.detect(name)
      version = detect_from_file("#{name}/#{name}.csproj", /<Version>([-\w\d.]+)</)
      return version if version.length.positive?
      version = detect_from_file("src/#{name}/#{name}.csproj", /<Version>([-\w\d.]+)</)
      return version if version.length.positive?

      Dir.glob("**/*.csproj").each { |csproj|
        version = detect_from_file(csproj, /<Version>([-\w\d.]+)</)
        return version if version.length.positive?
      }

      version = detect_from_file("#{name}/Properties/AssemblyInfo.cs", /^\[assembly: AssemblyVersion\("([.\w]+)/)
      return version if version.length.positive?

      version = detect_from_file("Cargo.toml", /version\s+=\s+['"]([\w.]+)['"]/)
      return version if version.length.positive?

      version = detect_from_file("#{name}.gemspec", /version\s+=\s+['"]([\w.]+)['"]/)
      return version if version.length.positive?

      version = detect_from_file("#{name}.gemspec", /version\s?=\s?['|"]([.\d]+)['|"]/)
      return version if version.length.positive?

      version = detect_from_file("#{name}.nuspec", /<[Vv]ersion>([\d.]+)</)
      return version if version.length.positive?
      ""
    end

    def self.detect_from_file(filename, regex)
      version = ""
      if File.exist?(filename)
        match = IO.read(filename).match(regex)
        version = match.captures[0] if !match.nil? && match.captures.length.positive?
      else
        return ""
      end
      version
    end

    def self.set_version_in_glob(glob_pattern, version)
      Dir.glob(glob_pattern).each { |f|
        Raykit::Version::set_version_in_file(f, version)
      }
    end

    def self.set_version_in_file(filename, version)
      text = IO.read(filename)
      new_text = text
      new_text = text.gsub(/version\s?=\s?['|"]([.\d]+)['|"]/, "version='#{version}'") if filename.include?(".gemspec")
      new_text = text.gsub(/<Version>([-\w\d.]+)</, "<Version>#{version}<") if filename.include?(".csproj")
      new_text = text.gsub(/<version>([-\w\d.]+)</, "<version>#{version}<") if filename.include?(".nuspec")
      new_text = text.gsub(/ Version="([\d\.]+)"/, " Version=\"#{version}\"") if filename.include?(".wxs")

      new_text = text.gsub(/version\s+=\s+['"]([\w.]+)['"]/, "version=\"#{version}\"") if filename.include?(".toml")

      # new_text=text.gsub(/<Version>([-\w\d.]+)</,"<Version>#{version}<")
      # new_text=new_text.gsub(/version[\s]+=\s?['|"]([.\d]+)['|"]/,"version='#{version}'")
      # new_text=new_text.gsub(/Version="([.\d]+)"/,"Version=\"#{version}\"")
      File.open(filename, "w") { |f| f.write(new_text) } if new_text != text
    end

    def self.sync_file_versions(source_filename, destination_filename)
      version = Version.detect_from_file(source_filename, /<Version>([-\w\d.]+)</, false)
      Version.set_version_in_file(destination_filename, version)
    end

    # increment to last digit of a version string
    def self.bump(version)
      # major.minor[.build[.revision]]  (example: 1.2.12.102)
      version_ints = version.split(".").map(&:to_i)
      version_ints[-1] = version_ints.last + 1
      version_ints.map(&:to_s).join(".")
    end

    def self.bump_file(filename)
      warn "Raykit::Version.bump_file filename '#{filename}' does not exist." unless File.exist?(filename)

      old_version = ""
      if filename.include?(".gemspec")
        old_version = detect_from_file(filename, /version\s?=\s?['|"]([.\d]+)['|"]/)
      end
      old_version = detect_from_file(filename, /<Version>([-\w\d.]+)</) if filename.include?(".csproj")

      if old_version.length.positive?
        new_version = bump(old_version)
        set_version_in_file(filename, new_version)
      end

      new_version
    end
  end
end

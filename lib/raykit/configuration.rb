require "json"

module Raykit
  class Configuration
    attr_accessor :root_dir, :auto_setup, :backup_dir, :log_level

    # Define a subdirectory and filename for the config file in a cross-platform manner.
    CONFIG_DIR = File.join(Dir.home, ".config", "raykit")
    CONFIG_FILE = File.join(CONFIG_DIR, "config.json")

    def initialize
      if File.exist?(CONFIG_FILE)
        load_configuration
      else
        set_default_configuration
        if (!Dir.exist?(CONFIG_DIR))
          save_configuration # Save the default configuration if no configuration file exists.
        end
      end
    end

    def load_configuration
      config_data = JSON.parse(File.read(CONFIG_FILE))
      @root_dir = config_data["root_dir"]
      @auto_setup = config_data["auto_setup"]
      @backup_dir = config_data["backup_dir"]
      @log_level = config_data["log_level"]
    end

    def set_default_configuration
      @root_dir = ""
      @auto_setup = false
      @backup_dir = "backup"
      @log_level = ::Logger::DEBUG
    end

    def save_configuration
      # Create the config directory if it doesn't exist.
      FileUtils.mkdir_p(CONFIG_DIR) unless Dir.exist?(CONFIG_DIR)

      File.write(CONFIG_FILE, {
        root_dir: @root_dir,
        auto_setup: @auto_setup,
        backup_dir: @backup_dir,
        log_level: @log_level,
      }.to_json)
    end

    def to_s
      "Root Directory: #{@root_dir}\nAuto Setup: #{@auto_setup}\nBackup Directory: #{@backup_dir}"
    end
  end # class Configuration
end # module Raykit

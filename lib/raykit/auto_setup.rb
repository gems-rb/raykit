module Raykit
  class AutoSetup
    def self.run
      PROJECT.set_version(VERSION) if (defined?(VERSION))
      # Default subdirectories
      DEFAULT_SUBDIRECTORIES.each do |subdirectory|
        puts "  RAYKIT_AUTO_SETUP: creating #{subdirectory}" unless Dir.exist?(subdirectory)
        FileUtils.mkdir_p(subdirectory) unless Dir.exist?(subdirectory)
      end
      puts "  RAYKIT_AUTO_SETUP: creating .gitignore" unless File.exist?(".gitignore")

      # Default .gitignore
      File.write(".gitignore", DEFAULT_GITIGNORE_CONTENT) unless File.exist?(".gitignore")

      #RAYKIT_CSHARP_PROJECTS = [
      #  ["classlib","src","MyCSharpLib",["Interfaces","Models"]],
      #  ["nunit","test","MyCSharpLib.Tests",["Interfaces","Models"]],
      #  ["razorclasslib","src","MyRazorComponents",["Pages","Components"]],
      #  ["nunit","test","MyRazorComponents.Tests",["Pages","Components"]],
      #  ["blazorserver","examples","MyBlazorServerApp",["Pages","Components"]]
      #]
      # C# projects
      if (defined?(RAYKIT_CSHARP_PROJECTS))
        RAYKIT_CSHARP_PROJECTS.each do |project|
          Raykit::DotNet::initialize_csharp_project project[0], project[1], project[2], project[3]
        end
      end

      # C# class libs
      if (defined?(RAYKIT_CSHARPCLASSLIBS))
        RAYKIT_CSHARPCLASSLIBS.each do |csharpclasslib|
          Raykit::DotNet::initialize_csharp_razorclasslib csharpclasslib
        end
      end

      # Razor class libs
      if (defined?(RAYKIT_RAZORCLASSLIBS))
        RAYKIT_RAZORCLASSLIBS.each do |razorclasslib|
          Raykit::DotNet::initialize_csharp_razorclasslib razorclasslib
        end
      end

      # Blazor server apps
      if (defined?(RAYKIT_BLAZORSERVERAPPS))
        RAYKIT_BLAZORSERVERAPPS.each do |app|
          Raykit::DotNet::initialize_csharp_blazorserver app
        end
      end

      # Blazor server example apps
      if (defined?(RAYKIT_BLAZORSERVER_EXAMPLE_APPS))
        RAYKIT_BLAZORSERVER_EXAMPLE_APPS.each do |app|
          Raykit::DotNet::initialize_csharp_blazorserver_example app
        end
      end

      # WPF example apps
      if (defined?(RAYKIT_WPF_EXAMPLE_APPS))
        RAYKIT_WPF_EXAMPLE_APPS.each do |app|
          Raykit::DotNet::initialize_csharp_wpf_example app
        end
      end

      # count the number of projects in the current directory
      project_count = Dir.glob("**/*.csproj").length
      Raykit::DotNet::create_solution(PROJECT.name) if (project_count > 0)
    end # def self.run
  end # class AutoSetup
end # module Raykit

module Raykit
  module Conan
    # Functionality to manage a git commit
    class BuildInfo < Hash
      def initialize(filename)
        current_section = ""
        current_list = Array.new()
        File.readlines(filename).each do |line|
          #puts line
          if (line.index("[") == 0)
            if (current_section.length > 0)
              self.store(current_section, current_list)
              current_list = Array.new()
            end
            current_section = line.strip()
          else
            item = line.strip()
            if (item.length > 0)
              current_list << item
            end
          end
          #
        end
      end

      def section(name)
        items = Array.new()
        if (self.has_key?(name))
          return self[name]
        end
        items
      end

      def pdbs
        items = Array.new()
        self.section("[builddirs]").each { |dir|
          Dir.glob("#{dir}/**/*.pdb").sort.each { |pdb|
            items << pdb
          }
        }
        items
      end

      def copy_pdbs(dir)
        FileUtils.mkdir_p(dir) if !Dir.exist?(dir)
        self.pdbs.each { |pdb|
          target = "#{dir}/#{File.basename(pdb)}"
          if (!File.exist?(target))
            puts "  copying #{pdb} to #{target}"
            FileUtils.cp(pdb, "#{target}")
          end
        }
      end

      def source_dirs(pattern)
        items = Array.new()
        self.section("[builddirs]").each { |dir|
          if (dir.include?(pattern))
            parts = dir.split("package/")
            if (parts.length == 2)
              items << "#{parts[0]}source"
            end
          end
        }
        items
      end
    end
  end
end

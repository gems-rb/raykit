# frozen_string_literal: true

module Raykit
  class NuGetPackage
    attr_accessor :name, :version

    def initialize(name, version)
      @name = name
      @version = version
    end

    def self.get(text, name)
      Raykit::NugetPackage.new(name, get_version(text, name))
    end

    def self.get_version(text, name)
      [/<PackageReference[\s]+Include=\"#{name}\"[\s]+Version=\"([\d\.]+)/,
       /<Reference[\s]+Include=\"#{name},[\s]+Version=([\d\.]+)/,
       /<HintPath>[\.\\\/\w\d]+#{name}.([\d\.]+)/].each { |regex|
        matches = text.scan(regex)
        if matches.length > 0 && matches[0].length > 0
          return matches[0][0]
        end
      }
      ""
    end

    def self.set_version(text, name, version)
      puts "NuGetPackage::set_version"
      new_text = text
      [/(<PackageReference[\s]+Include=\"#{name}\"[\s]+Version=\"[\d\.]+)/,
       /(<Reference[\s]+Include=\"#{name},[\s]+Version=[\d\.]+)/,
       /(<HintPath>[\.\\\/\w\d]+#{name}.[\d\.]+)/].each { |regex|
        matches = text.scan(regex)
        if matches.length > 0 && matches[0].length > 0
          orig = matches[0][0]
          oversion = get_version(orig, name)
          mod = orig.sub(oversion, version)
          puts "match[0][0] " + matches[0][0]
          puts "old version " + oversion
          puts "new version " + version
          new_text = new_text.gsub(orig, mod)
        end
      }
      new_text
    end

    def self.set_version_in_file(filename, name, version)
      text = set_version(IO.read(filename), name, version)
      orig = IO.read(filename)
      File.open(filename, "w") { |f| f.puts text } if (text != orig)
    end
  end
end

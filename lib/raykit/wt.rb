module Raykit
  class Wt
    def self.open(names)
      if names.is_a? String
        cmd = Raykit::Command.new("wt --maximized #{get_tab_arg(names)}").run
      end
      if names.is_a?(Array)
        cmd = "wt --maximized "
        index = 0
        names.each { |url|
          if index <= 0
            cmd += get_tab_arg(url)
          else
            cmd += ";new-tab " + get_tab_arg(url)
          end
          index = index + 1
        }
        Raykit::Command.new(cmd).run
      end
    end

    def self.get_tab_arg(url)
      dir = Raykit::Git::Repository.new(url).get_dev_dir("work")
      name = File.basename(dir)
      "-d #{dir} --title #{name}"
    end
  end
end

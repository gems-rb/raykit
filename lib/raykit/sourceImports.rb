# frozen_string_literal: true

module Raykit
  class SourceImports < Array
    def initialize(urls)
      urls.each do |url|
        self << SourceImport.new(url, "src", "**/*.cs", "dep", "")
      end
    end

    def update
      each(&:update)
    end

    def copy
      each(&:copy)
    end

    def targets_exist?
      each do |si|
        return false unless Dir.exist?(si.target)
      end
      true
    end

    def save(filename)
      File.open(filename, "w") do |f|
        f.write(JSON.pretty_generate(self))
      end
    end

    def self.load(filename)
      sourceImports = SourceImports.new([])
      array = JSON.parse(IO.read(filename))
      array.each do |hash|
        sourceImport = SourceImport.new(hash["remote"], hash["source"], hash["glob"], hash["target"],
                                        hash["commit"])
        sourceImports << sourceImport
      end
      sourceImports
    end
  end
end

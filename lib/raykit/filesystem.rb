module Raykit
  class FileSystem
    def self.copy_files(source_dir, target_dir, glob_pattern)
      FileUtils.mkdir_p(target_dir) if !Dir.exist?(target_dir)
      #Dir.chdir(source_dir) do
      file_count = 0
      Dir.glob("#{source_dir}/#{glob_pattern}").each { |f|
        rel = f.gsub("#{source_dir}/", "")
        dest = "#{target_dir}/#{rel}"
        FileUtils.mkdir_p(File.dirname(dest)) if (!Dir.exist?(File.dirname(dest)))
        #puts "  copying #{rel} to #{dest}"
        FileUtils.cp(f, dest)
        file_count = file_count + 1
      }
      #end
      puts "  copied " + file_count.to_s + " file(s) from #{source_dir} to #{target_dir}"
    end

    def self.copy_file_to_dir(file, dir)
      dest = "#{dir}/#{File.basename(file)}"
      if (File.exist?(dest))
        return "#{dest} already exists"
      else
        FileUtils.cp(file, dest)
        return "#{dest} now exists"
      end
    end

    def self.replace_invalid_chars(str)
      #[\\/:"*?<>|]
      str.gsub('\\', "_").gsub("/", "_").gsub(":", "_").gsub("*", "_").gsub("?", "_")
    end
  end
end

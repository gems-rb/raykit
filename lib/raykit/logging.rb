# frozen_string_literal: true

require "json"
require "logger"

module Raykit
  class Logging
    attr_accessor :enabled, :loggers

    # Logger::Severity DEBUG,ERROR,FATAL,INFO,UNKOWN,WARN
    # defaults to WARN
    attr_accessor :severity

    def initialize
      @enabled = true
      @loggers = {}
      @severity = ::Logger::Severity::WARN
    end

    def set_severity_as_string(severity)
      @severity = ::Logger::Severity::DEBUG if severity == "debug"
      @severity = ::Logger::Severity::INFO if severity == "info"
      @severity = ::Logger::Severity::WARN if severity == "warn"
      @severity = ::Logger::Severity::ERROR if severity == "error"
    end

    def get_logger(context)
      unless loggers.key?(context)
        Dir.chdir(Environment.get_dev_dir("log")) do
          # start the log over whenever the log exceeds 100 megabytes in size
          loggers[context] = Logger.new("#{context}.log", 0, 100 * 1024 * 1024)
        end
      end
      loggers[context]
    end

    def log(context, level, message)
      if @enabled
        logger = get_logger(context)
        case level
        when Logger::Severity::DEBUG
          logger.debug(message)
        when Logger::Severity::INFO
          logger.info(message)
        when Logger::Severity::WARN
          logger.warn(message)
        when Logger::Severity::ERROR
          logger.error(message)
        when Logger::Severity::FATAL
          logger.fatal(message)
        else
          logger.unknown(message)
        end
      end
    end
  end
end

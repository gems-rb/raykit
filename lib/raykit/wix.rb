# frozen_string_literal: true

module Raykit
  class Wix
    def self.harvest_directory_elements(directory)
      get_nested_directory_element("", directory)
    end
    def self.get_nested_directory_element(id_prefix, directory)
      Dir.chdir(directory) do
        results = Array::new
        Dir.glob("*").reject { |f| !File.directory? f }.each { |d|
          directoryId = "#{d.upcase.gsub("/", "_").gsub("-", "_")}DIR"
          directoryId = "#{id_prefix}#{directoryId}"
          child_elements = get_nested_directory_element("#{directoryId[0...-3]}_", d)
          if (child_elements.length > 0)
            results.push("<Directory Id=\"#{directoryId}\" Name=\"#{d}\">")
            child_elements.each { |child_element|
              results.push("\t#{child_element}")
            }
            results.push("</Directory>")
          else
            results.push("<Directory Id=\"#{directoryId}\" Name=\"#{d}\"/>")
          end
        }
        results
      end
    end
    # given a directory, return an array of string the represent wix components for each file in the directory
    # each string should look like this:
    # <Component Id="IndexHtmlComponent" Directory="WWWDIR" Guid="*">
    #	<File Id="IndexHtmlFile" Source="path\to\index.html" KeyPath="yes"/>
    # </Component>
    def self.harvest_file_components(directory)
      components = Array::new
      Dir.chdir(directory) do
        index = 0
        Dir.glob("**/*").reject { |f| File.directory? f }.each { |f|
          index += 1
          component = get_indexed_file_component(f, index)
          components.push(component)
        }
        components
      end
    end
    # given a file and an index, return a string that represents a wix component
    def self.get_indexed_file_component(file, index)
      componentId = "c#{index}"
      guid = SecureRandom.uuid
      fileId = "f#{index}"
      directory = File.dirname(file)
      directory = directory.split(/[\/\\]/).last if File.dirname(file).include?("\\") || File.dirname(file).include?("/")
      directory = "" if directory == "."
      directoryId = "#{File.dirname(file).upcase.gsub("/", "_").gsub("-", "_")}DIR"
      component = "<Component Id=\"#{componentId}\" Guid=\"#{guid}\"><File Id=\"#{fileId}\" Source=\"#{file}\" /></Component>"
      if (directory.length > 0)
        component = "<Component Id=\"#{componentId}\" Guid=\"#{guid}\" Directory=\"#{directoryId}\"><File Id=\"#{fileId}\" Source=\"#{file}\" /></Component>"
      end
      component
    end
  end
end

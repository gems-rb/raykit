# frozen_string_literal: true

require "tmpdir"
require "find"
require_relative "./environment"

class Dir
  def self.get_text(dir, relative_name)
    File.read("#{dir}/#{relative_name}")
  end

  def self.set_text(dir, relative_name, text)
    File.open("#{dir}/#{relative_name}", "w") { |f| f.write text }
  end

  def self.directory_size(directory)
    total_size = 0
    Find.find(directory) do |path|
      if File.file?(path)
        total_size += File.size(path)
      end
    end
    total_size
  end

  def self.humanize_size(size_in_bytes)
    units = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB"]

    return "0 Bytes" if size_in_bytes == 0

    exponent = (Math.log(size_in_bytes) / Math.log(1024)).to_i
    exponent = units.size - 1 if exponent > units.size - 1 # Cap at the largest known unit

    size = size_in_bytes.to_f / (1024 ** exponent)
    format("%.2f %s", size, units[exponent])
  end

  def self.get_git_directories(dir)
    git_dirs = []
    Dir.chdir(dir) do
      Dir.glob("**/.git/index") do |f|
        git_dir = File.dirname(File.dirname(f))
        git_dirs << git_dir
      end
    end
    git_dirs
  end

  def self.get_git_urls(dir)
    urls = Set.new
    Dir.chdir(dir) do
      get_git_directories(dir).each do |git_dir|
        Dir.chdir(git_dir) do
          url = `git config --get remote.origin.url`.strip
          urls.add(url)
        end
      end
    end
    urls
  end

  def self.get_data_dir
    unless ENV["DATA_DIR"].nil?
      data_dir = ENV["DATA_DIR"]
      return data_dir if Dir.exist?(data_dir)
    end
    home_dir = Raykit::Environment.home_dir
    return home_dir if Dir.exist?(home_dir)

    Dir.tmpdir
  end

  require "fileutils"

  def self.remove_empty_directories(dir)
    # List all entries in the directory except for '.' and '..'
    Dir.entries(dir).each do |entry|
      next if entry == "." || entry == ".."  # Skip the current and parent directory entries

      path = File.join(dir, entry)  # Construct the full path

      if File.directory?(path)
        begin
          remove_empty_directories(path)  # Recursively call the method if the entry is a directory
        rescue Errno::ENAMETOOLONG
          puts "Path too long: #{path}"
          next  # Skip this directory and continue with others
        end

        # Attempt to remove the directory if it's empty after processing its contents
        begin
          Dir.rmdir(path) if Dir.empty?(path)
        rescue Errno::ENAMETOOLONG
          puts "Path too long to remove: #{path}"
        end
      end
    end
  rescue Errno::ENOENT
    # Handle the case where the directory doesn't exist or is removed before rmdir is called
    puts "Directory not found: #{dir}"
  rescue Errno::ENAMETOOLONG
    # Handle the case where the initial directory path is too long
    puts "Initial path too long: #{dir}"
  end

  #def self.remove_empty_directories(dir)
  # List all entries in the directory except for '.' and '..'
  #  Dir.entries(dir).each do |entry|
  #    next if entry == "." || entry == ".."  # Skip the current and parent directory entries

  #    path = File.join(dir, entry)  # Construct the full path

  #    if File.directory?(path)
  #      remove_empty_directories(path)  # Recursively call the method if the entry is a directory
  # Remove the directory if it's empty after processing its contents
  #      Dir.rmdir(path) if Dir.empty?(path)
  #    end
  #  end
  #rescue Errno::ENOENT
  # Handle the case where the directory doesn't exist or is removed before rmdir is called
  #  puts "Directory not found: #{dir}"
  #end

  # Example usage:
  # remove_empty_directories('/path/to/directory')

end

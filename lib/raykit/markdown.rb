module Raykit
  class Markdown
    attr_accessor :text

    def initialize()
      @text = ""
    end

    def puts(line)
      @text = @text + "\n" + line
    end

    def to_s()
      @text
    end

    def save_as(filename)
      #File.write("rake.md", @text)
    end
  end
end

# frozen_string_literal: true

require "yaml"

module Raykit
  class Runner
    def self.run(git_url)
      commands = []
      local_dir = Dir.mktmpdir("runner")
      puts "local_dir : #{local_dir}"
      commands << Raykit::Command.new("git clone #{git_url} #{local_dir}")
      Dir.chdir(local_dir) do
        commands << Raykit::Command.new("git log -n 1")
        yaml = get_build_yaml(local_dir)
        build_hash = YAML.safe_load(yaml)
        build_commands = Raykit::Command.parse_yaml_commands(yaml)
        if build_hash.key?("image")
          image = build_hash["image"]
          build_commands.insert(0, "cd home")
          build_commands.insert(1, "git clone #{git_url} build")
          build_commands.insert(2, "cd build")
          build_commands_string = build_commands.join(";")
          commands << Raykit::Command.new("docker run #{image} sh -c \"#{build_commands_string}\"")
        else
          build_commands.each do |cmd_string|
            commands << Rakkit::Command.new(cmd_string)
          end
        end
      end
      FileUtils.rm_rf(local_dir)
      commands
    end

    def self.get_build_yaml(directory)
      yaml = ""
      Dir.chdir(directory) do
        yaml = File.open(".gitlab-ci.yml").read if File.exist?(".gitlab-ci.yml")
      end
      yaml
    end
  end
end

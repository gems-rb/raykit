# frozen_string_literal: true

require "time"

module Raykit
  # Provides functionality to record the time execution times
  class Secrets < Hash
    def initialize
      if ENV.key?("RAYKIT_SECRETS_PATH")
        secrets_file = ENV["RAYKIT_SECRETS_PATH"]
        if File.exist?(secrets_file)
          text = IO.read(secrets_file)
          if text.length > 7
            data = JSON.parse(text)
            data.each do |key, value|
              self[key] = value
            end
          end
        end
      end
    end

    def hide(text)
      hidden = text
      each do |_k, v|
        hidden = hidden.gsub(v, "****") if !v.nil? && v.length.positive?
      end
      hidden
    end

    def save
      if ENV.key?("RAYKIT_SECRETS_PATH")
        secrets_file = ENV["RAYKIT_SECRETS_PATH"]
        File.open(secrets_file, "w") { |f| f.puts to_json }
      end
    end
  end
end

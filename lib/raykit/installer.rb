# frozen_string_literal: true

module Raykit
  class Installer
    def self.make_msi(wxs_file, source_dir, msi_filename)
      name = "#{File.basename(wxs_file, ".wxs")}"
      FileUtils.cp(wxs_file, "#{source_dir}/#{File.basename(wxs_file)}")
      Dir.chdir(source_dir) do
        run("candle #{File.basename(wxs_file)}")
        run("light #{name}.wixobj")
        FileUtils.cp("#{name}.msi", msi_filename)
        raise "#{msi_filename} does not exist" if !File.exist?(msi_filename)
        File.delete("#{name}.wixobj")
      end
    end
  end
end

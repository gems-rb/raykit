# frozen_string_literal: true

module Raykit
  class MsBuild
    def self.fix_msbuild_path
      # if msbuild in not in the current path,
      # attempt to modify the path such that is it
      has_msbuild = false
      begin
        cmd = Raykit::Command.new("msbuild --version").run
        has_msbuild = true if (cmd.exitstatus != 0)
      rescue
        has_msbuild = false
      end
      if (!has_msbuild)
        if (Dir.exist?(msbuild_path))
          #puts "  added #{msbuild_path} to PATH for msbuild"
          ENV["PATH"] = ENV["PATH"] + ";#{msbuild_path}"
        end
      end
    end

    # C:\Program Files\Microsoft Visual Studio\2022\Community\Msbuild\Current\Bin
    def self.msbuild_path
      ["2022/Community/Msbuild/Current/Bin",
       "2022/Professional/Msbuild/Current/Bin",
       "2022/Enterprise/Msbuild/Current/Bin",
       "2019/Enterprise/MSBuild/Current/Bin",
       "2019/Professional/MSBuild/Current/Bin",
       "2019/Community/MSBuild/Current/Bin",
       "2017/BuildTools/MSBuild/15.0/Bin"].each do |relative_path|
        ["C:/Program Files/Microsoft Visual Studio/",
         "C:/Program Files (x86)/Microsoft Visual Studio/"].each do |prog_path|
          path = "#{prog_path}#{relative_path}"
          return path if Dir.exist?(path)
        end
      end
      ""
    end

    def self.msbuild_2019_path
      ["2019/Enterprise/MSBuild/Current/Bin",
       "2019/Professional/MSBuild/Current/Bin",
       "2019/Community/MSBuild/Current/Bin"].each do |relative_path|
        ["C:/Program Files/Microsoft Visual Studio/",
         "C:/Program Files (x86)/Microsoft Visual Studio/"].each do |prog_path|
          path = "#{prog_path}#{relative_path}"
          return path if Dir.exist?(path)
        end
      end
      ""
    end
  end
end

module Raykit
  class DefaultContent
    def self.gitignore
      file_path = File.expand_path("../default_content.txt", __FILE__)
      File.read(file_path)
    end
  end
end

require "logger"
require "rainbow"

module Raykit
  class Logger
    def initialize(logger_array)
      @logger_array = logger_array
      #@loggerB = loggerB
    end

    def level=(level)
      @logger_array.each do |logger|
        logger.level = level
      end
    end

    def debug(message)
      @logger_array.each do |logger|
        logger.debug(message)
      end
    end

    def info(message)
      @logger_array.each do |logger|
        logger.info(message)
      end
    end

    def warn(message)
      @logger_array.each do |logger|
        logger.warn(message)
      end
    end

    def error(message)
      @logger_array.each do |logger|
        logger.error(message)
      end
    end

    def fatal(message)
      @logger_array.each do |logger|
        logger.fatal(message)
      end
    end

    def self.strip_ansi_codes(str) # Strips ANSI color codes
      str.gsub(/\e\[[0-9;]*m/, "")
    end

    def self.default
      Raykit::Logger.new([Raykit::Logger::console_logger]) #, Raykit::Logger::file_logger])
    end

    def self.console_logger
      console_logger = ::Logger.new(STDOUT)
      console_logger.formatter = proc do |_severity, _datetime, _progname, msg|
        "#{Raykit::Symbols::get_severity_symbol(_severity)} #{msg}\n"
      end
      console_logger
    end

    def self.file_logger
      log_dir = Environment::log_dir
      FileUtils.mkdir_p(log_dir) if (!Dir.exist?(log_dir))
      #FileUtils.mkdir("log") if (!Dir.exist?("log"))
      file_logger = ::Logger.new(File.new(LOG_FILENAME, "w"))
      file_logger.formatter = proc do |_severity, _datetime, _progname, msg|
        "#{strip_ansi_codes(msg)}\n"
      end
      file_logger
    end
  end # class Logger
end # module Raykit

DEFAULT_LOGGER = Raykit::Logger::default

def set_log_level(level)
  DEFAULT_LOGGER.level = level
end

def log_debug(message)
  DEFAULT_LOGGER.debug(message)
end

def log_info(message)
  DEFAULT_LOGGER.info(message)
end

def log_warn(message)
  DEFAULT_LOGGER.warn(message)
end

def log_error(message)
  DEFAULT_LOGGER.error(message)
end

def log_fatal(message)
  DEFAULT_LOGGER.fatal(message)
end

# frozen_string_literal: true

module Raykit
  class VsTest
    def self.vstest_path
      ["2022/Community/Common7/IDE/CommonExtensions/Microsoft/TestWindow",
       "2022/Professional/Common7/IDE/CommonExtensions/Microsoft/TestWindow",
       "2022/Enterprise/Common7/IDE/CommonExtensions/Microsoft/TestWindow",
       "2019/Community/Common7/IDE/CommonExtensions/Microsoft",
       "2019/Professional/Common7/IDE/Extensions/TestPlatform",
       "2019/Community/Common7/IDE/Extensions",
       "2019/Community/Common7/IDE/Extensions/TestPlatform",
       "2022/Preview/Common7/IDE/Extensions/TestPlatform"].each do |relative_path|
        ["C:/Program Files (x86)/Microsoft Visual Studio/",
         "C:/Program Files/Microsoft Visual Studio/"].each do |root_path|
          path = root_path + relative_path
          exe_path = "#{path}/vstest.console.exe"
          return path if Dir.exist?(path) && File.exist?(exe_path)
        end
      end
      "vstest_path not found"
    end
  end
end

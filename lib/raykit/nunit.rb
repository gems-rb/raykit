module Raykit
  class NUnit
    def self.nunit3console_path(version)
      # C:\Users\lparslow\.nuget\packages\nunit.consolerunner\3.15.0\tools
      path = "#{Environment.home_dir}/.nuget/packages/nunit.consolerunner/#{version}/tools"
      if Dir.exist?(path)
        path
      else
        ""
      end
    end
  end
end

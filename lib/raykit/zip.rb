require "zip"

module Raykit
  class Zip
    @filename
    @source_dir
    @include_globs
    @exclude_globs

    def initialize(filename)
      @filename = filename
      @source_dir = Dir.pwd
      @include_globs = []
      @exclude_globs = []
      self
    end

    def source_dir(dir)
      @source_dir = dir
      self
    end

    def include_glob(glob)
      @include_globs << glob
      self
    end

    def exclude_glob(glob)
      @exclude_globs << glob
      self
    end

    def zip
      path = File.dirname(@filename)
      FileUtils.mkdir_p(path) unless Dir.exist?(path)

      files_to_archive = Array::new()
      Dir.chdir(@source_dir) do
        #include_files = []
        @include_globs.each do |include_glob|
          Dir.glob(include_glob) { |f|
            #puts "\n" + f
            files_to_archive << f if File.file?(f)
          }
        end
      end

      ::Zip::File::open(@filename, ::Zip::File::CREATE) { |zipfile|
        count = 0
        files_to_archive.each do |file|
          zipfile.get_output_stream(file) { |f|
            fr = ::File.open("#{@source_dir}/#{file}", "rb")
            f.puts(fr.read)
            fr.close()
            f.close()
            count = count + 1
          }
        end
        zipfile.close
        puts "  added " << count.to_s << " files to " << @filename
      }
    end

    def self.zip_directory(directory, zipfile_name, overwrite = true)
      File.delete(zipfile_name) if File.exist?(zipfile_name) && overwrite
      ::Zip::File.open(zipfile_name, ::Zip::File::CREATE) do |zipfile|
        Dir[File.join(directory, "**", "**")].each do |file|
          zipfile.add(file.sub(directory + "/", ""), file)
        end
      end
    end
  end
end

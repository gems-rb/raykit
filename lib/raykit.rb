# frozen_string_literal: true

require "rainbow"
require "rake/clean"
require "open3"
require "rake/testtask"
require "digest"

# Constants
BUFFER_SIZE = 1024 unless defined?(BUFFER_SIZE)
RAKE_DIRECTORY = Rake.application.original_dir unless defined?(RAKE_DIRECTORY)
DEFAULT_SUBDIRECTORIES = %w[src test examples] unless defined?(DEFAULT_SUBDIRECTORIES)
DEFAULT_FILES = %w[README.md LICENSE.md .gitignore] unless defined?(DEFAULT_FILES)
RAYKIT_GLOBALS = true unless defined?(RAYKIT_GLOBALS)
RAYKIT_AUTO_SETUP = false unless defined?(RAYKIT_AUTO_SETUP)
LOG_FILENAME = ARGV.empty? ? "log/rake.txt" : "log/rake.#{ARGV.join(".")}.txt"

# Load all the files in the raykit directory

lib_dir = File.dirname(__FILE__)
Dir.glob("#{lib_dir}/raykit/**/*.rb").sort.each { |file| require file }

# Constants
DEV_ROOT = Raykit::Environment::root_dir
#DEFAULT_GITIGNORE_CONTENT = Raykit::DefaultContent::gitignore unless defined?(DEFAULT_GITIGNORE_CONTENT)
unless defined?(VERSION)
  VERSION = Raykit::Version.detect(File.basename(RAKE_DIRECTORY))
end
unless defined?(RUNTIME_IDENTIFIER)
  RUNTIME_IDENTIFIER = Raykit::DotNet::get_runtime_identifier()
end
PROJECT = Raykit::Project.new
SECRETS = Raykit::Secrets.new
REPOSITORIES = Raykit::Git::Repositories.new("#{Raykit::Environment.get_dev_dir("log")}/Raykit.Git.Repositories.json")
GIT_DIRECTORY = Raykit::Git::Directory.new(Rake.application.original_dir)
NUGET_DIR = Raykit::Environment::get_dev_dir("nuget")
PUBLISH_DIR = Raykit::Environment::get_dev_dir("publish")
ARTIFACTS_DIR = Raykit::Environment::get_dev_dir("artifacts")

LAST_MODIFIED_SOURCE_FILE = GIT_DIRECTORY.last_modified_src_file
LAST_MODIFIED_SOURCE_TIME = GIT_DIRECTORY.last_modified_src_time
LAST_MODIFIED_FILE = GIT_DIRECTORY.last_modified_filename
LAST_MODIFIED_TIME = GIT_DIRECTORY.last_modified_time
LAST_MODIFIED_ARTIFACT_TIME = GIT_DIRECTORY.last_modified_artifact_time

LOG = Raykit::Logging.new
MARKDOWN = Raykit::Markdown.new

Raykit::MsBuild::fix_msbuild_path

File.write(".gitignore", Raykit::DefaultContent::gitignore) unless File.exist?(".gitignore")
Raykit::AutoSetup.run if RAYKIT_AUTO_SETUP

def backup_git_directory
  if ENV["GIT_BACKUP_DIR"] && !ENV["GIT_BACKUP_DIR"].empty?
    puts "Backing up #{GIT_DIRECTORY.repository.url}"
    Raykit::Git::Repository::backup GIT_DIRECTORY.repository.url, "#{ENV["GIT_BACKUP_DIR"]}/#{GIT_DIRECTORY.repository.relative_path}"
  else
    puts "Environment variable GIT_BACKUP_DIR is not set"
  end
end

# include Raykit::TopLevel to make run method accessible from the global scope
module Raykit
  module TopLevel
    def self.run(command, quit_on_failure = true)
      PROJECT.run(command, quit_on_failure)
    end
  end
end

if defined?(RAYKIT_GLOBALS)
  def run(command, quit_on_failure = true)
    Raykit::TopLevel.run(command, quit_on_failure)
  end

  def run_details(command)
    cmd = Raykit::Command.new(command).run
    cmd.details
  end

  def try(command)
    Raykit::TopLevel.run(command, false)
  end

  def dir(name)
    FileUtils.mkdir("artifacts") if (!Dir.exist?(name))
  end

  def make(artifact, command)
    if (File.exist?(artifact))
      puts "  #{artifact} exists"
    else
      cmd = run(command)
      if (cmd.exitstatus != 0)
        File.delete(artifact) if (File.exist?(artifact))
      end
      cmd
    end
  end

  def make_log(artifact, command)
    if (File.exist?(artifact))
      puts "  #{artifact} exists"
    else
      cmd = run(command).log_to_file(artifact)
      if (cmd.exitstatus != 0)
        File.delete(artifact) if (File.exist?(artifact))
      end
      cmd
    end
  end

  def tag(name)
    puts Rainbow(": #{name}").blue.bright
  end

  def start_task(task_name)
    Raykit::Log.start_task(task_name)
  end

  def show(symbol)
    show_binding(symbol, binding)
  end

  def show_binding(symbol, the_binding)
    show_value symbol.to_s, eval(symbol.to_s, the_binding)
  end

  def show_value(name, value)
    Raykit::Log.show_value(name, value)
    PROJECT.values[name] = value
  end

  def show_table(symbols)
    Raykit::Log.show_table(symbols)
  end

  def copy_files(src_dir, target_dir, glob)
    Raykit::FileSystem::copy_files(src_dir, target_dir, glob)
  end

  def copy_file_to_dir(file, dir)
    Raykit::FileSystem::copy_file_to_dir(file, dir)
  end
end # if defined?(RAYKIT_GLOBALS)

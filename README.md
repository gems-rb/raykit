# raykit

ruby gem supporting rake ci/cd tasks

## raykit console application

# TODO
- consider using https://github.com/tj/terminal-table to display name value pairs

```secrets.txt
{"test":"abc123XYZ"}
```

```ruby
ENV['RAYKIT_SECRETS_PATH'] = SOME_VALID_FILENAME
if (SECRETS.has_key?("test"))
  puts "found SECRETS[\"test\"]"
else
  puts "did not find SECRETS[\"test\"]"
end
```


# raykit work sample output
```shell
```
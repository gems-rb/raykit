# examples/configuration.rb
$LOAD_PATH.unshift(File.join(__dir__, "..", "lib"))
require "raykit.rb"

puts "Gem.win_platform? #{Gem.win_platform?}"
#URL = "https://gitlab.com/gems-rb/raykit.git"
URL = "https://github.com/node-net/Node.Net.git"
repo = Raykit::Git::Repository.new(URL)
puts repo.to_s

puts repo.to_table

puts "\n#{repo.short_name} make \"rake default\""
cmd = repo.make "rake default"
puts cmd.to_s

puts "\n#{repo.short_name} work \"rake default\""
cmd = repo.work "rake default"
puts cmd.to_s

puts "\n#{repo.short_name} pull"
repo.pull

puts "\n#{repo.short_name} clobber"
repo.clobber

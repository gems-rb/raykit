# examples/configuration.rb
$LOAD_PATH.unshift(File.join(__dir__, "..", "lib"))
require "raykit/configuration" # Requiring the class from lib/configuration.rb

config = Raykit::Configuration.new
puts config.to_s
#puts "Root Dir Name: #{config.root_dir_short_name}"
#puts "Auto Setup: #{config.auto_setup}"
#puts "Backup Dir: #{config.backup_dir}"

# Modify and save configuration.
config.root_dir = "code"
config.auto_setup = true
config.backup_dir = "backup"
config.save_configuration

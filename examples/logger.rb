$LOAD_PATH.unshift(File.join(__dir__, "..", "lib"))
require "raykit.rb"

# Set the log level to DEBUG (lowest level)
set_log_level(Logger::DEBUG)

def format_task(task_name)
  Rainbow(": #{task_name}").blue.bright
end

# log constant values
log_info(format_task(:setup))
log_info("RAYKIT_GLOBALS #{RAYKIT_GLOBALS}")

#def self.start_task(task_name)
#    puts Rainbow(": #{task_name}").blue.bright
#    MARKDOWN.puts(": #{task_name}")
#  end

#  def self.show_value(name, value)
#    puts "  " + Rainbow(name).cyan + " " + Rainbow("#{value}").white.bold
#    MARKDOWN.puts("#{name} #{value}")
#  end
# puts "LOG_FILENAME #{LOG_FILENAME}"
# Log messages at various log levels
log_debug("This is a debug message")
log_info(Rainbow("This is an info message").yellow)

log_warn("This is a warning message")
log_error("This is an error message")
log_fatal("This is a fatal message")

# If we change the log level, messages below that level will be suppressed
set_log_level = Logger::WARN

puts "After changing the log level to WARN..."

log_debug("This debug message will not be shown")
log_info("This info message will not be shown")
log_warn("This warning message will still be displayed")
log_error("This error message will still be displayed")
log_fatal("This fatal message will still be displayed")
